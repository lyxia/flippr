{-|
The module implements type inference in FliPpr. 
-}

module Typing where 

{-
To know which arguments is either of:
(1) input tree, which can be pattern matched, but 
    called only by variables.
(2) context information, which can be pattern matched, 
    which cannot be pattern matched and 
    called only by context information variables or a constant.
(3) documents, which cannot be pattern matched, 
    can be an expression.    

Thus, we prepare the following types:
* Input    
* Context  
* Constant  
* Document 

There is a subtyping relation, Constant < Context 

This is a very rough type checker, and thus this only reports some errors, 
because the pretty-prenter we write does not know any types of ASTs. 
Also note that our type checker is monomorhpic.

-}

import Control.Monad.State 
import Data.List (groupBy)
import Data.Function (on)

import Data.Composable 
import Name 
import Syntax 

data Type 
    = TyInput
    | TyContext 
    | TyConstant -- it is used to ensure something must be a constant, not occuring in type env
    | TyCharSet 
    | TyDocument 
    | TyReg 
      deriving (Show,Eq)

data Constraint = CEq Ty Ty 
                | Cp  Ty -- a context or an input 

data Ty = TyVar Int 
        | Ty    Type 

-- TODO: The current error message is too bad.

primitiveMapping 
    = [ (Name n, [c,c], c) | n <- binOps ]
      ++ [ (Name "chars", [c],s) ]
      ++ [ (Name "char",  [c],s) ]
      ++ [ (Name "range",  [c,c],s) ]
      ++ [ (Name "not", [c],c) ]
      ++ [ (Name n, [s,s],s) | n <- binCOps ]
      ++ [ (Name n, [],s) | n <- nullCOps ]      
      ++ [ (Name n, [r,r],r) | n <- binROps ]
      ++ [ (Name "many", [r], r) ]
      ++ [ (Name "re", [c], r) ] 
    where
      binROps  = [ "\\\\", "union", "intersect", "followedBy" ]
      binOps   = [ "==", "/=", "<=", ">=", "<", ">", "&&", "||"]
      binCOps  = [ "andalso", "orelse", "butnot" ]
      nullCOps = [ "allChars", "noChars", 
                   "spaceChars", "alphaChars", "alphaNumChars", 
                   "lowerChars", "upperChars", "printChars", "digitChars", 
                   "octDigitChars", "hexDigitChars", "letterChars", 
                   "markChars", "numberChars", "punctuationChars", 
                   "symbolChars", "separatorChars", 
                   "asciiChars", "asciiLowerChars", "asciiUpperChars", 
                   "latin1Chars" ]
      -- nullCOps = [ "anyChar", "none"
      --            -- From Data.Char 
      --            , "isSpace", "isAlpha", "isAlphaNum"
      --            , "isLower", "isUpper", "isPrint", "isDigit"
      --            , "isOctDigit", "isHexDigit", "isLetter", "isMark"
      --            , "isNumber", "isPunctuation", "isSymbol", "isSeparator" ]
      r = Ty TyReg 
      c = Ty TyContext 
      d = Ty TyDocument 
      s = Ty TyCharSet 

gatherContraint :: Prog a -> ( [(Name,[Ty],Ty)], [Constraint])
gatherContraint prog = flip evalState 0 $
    do { fs <- mapM assignTyVars $ map head $ groupBy ((==) `on` (\(f,ps) -> f)) 
                                     [(f,ps) | Decl _ f ps e <- prog ]
       ; let fs' = fs ++ primitiveMapping
       ; cs <- mapM (gatherD fs') prog 
       ; mainC <- fromMain $ head [ (ts,t) | (f,ts,t) <- fs, f == mainFunc ]
       ; return (fs', mainC ++ concat cs) }
    where
      mainFunc = head $ [ f | Decl _ f _ _ <- prog ]

      fromMain ([t'],t) = 
          return [CEq t' (Ty TyInput), CEq t (Ty TyDocument)]
      fromMain _ =
          error $ "Main pretty-priting function ``" ++ show mainFunc 
                   ++ "'' must take *exactly one* argument"

      newTyVar :: State Int Ty
      newTyVar = do { i <- get; put (i+1); return $ TyVar i }

      assignTyVars (f,ps) = 
          do { i  <- newTyVar 
             ; is <- mapM (\_ -> newTyVar) ps 
             ; return $ (f,is,i) }

      gatherD fs (Decl _ f ps e) = 
          do { let (is,i) = head $ [ (is,i) | (g,is,i) <- fs, g == f ]
             ; (c1,envs,ts) <- fmap unzip3 $ mapM gatherP ps 
             ; (c2,t)     <- gatherE fs (concat envs) e
             ; return $ zipWith CEq ts is ++ (CEq t i : concat c1 ++ c2) }

      gatherP (PVar _ x) = do { j <- newTyVar
                              ; return ([], [(x,j)], j) }
      gatherP (PWild _)  = return ([],[], Ty TyInput) 
      gatherP (PCon _ x ps) = 
          do { j <- newTyVar 
             ; (cs,envs,ts) <- fmap unzip3 $ mapM gatherP ps 
             ; let cont = zipWith CEq ts (cycle [j]) ++ concat cs 
             ; return (cont, concat envs, j) }


          -- do { -- j <- newTyVar 
          --    ; cenvs <- mapM gatherP ps 
          --    ; let (cs,envs,ts) = unzip3 cenvs 
          --    -- ; return ( Cp j : map Cp ts ++ concat cs,
          --    --            concat envs, 
          --    --            j ) 
          --    ; return ( zipWith CEq ts (cycle [Ty TyInput]) ++ concat cs,
          --               concat envs, 
          --               Ty TyInput  )
          --    }

      gatherE fs env (EVar _ f es) =
          case lookup f env of 
            Just t | null es -> return ([],t) 
            Nothing ->
                case lookup f [ (f,(is,i)) | (f,is,i) <- fs ] of 
                  Just (is,i) | length is == length es -> 
                      do { (cs,ts) <- fmap unzip $ mapM (gatherE fs env) es
                         ; return (zipWith f ts is++concat cs, i) }
                      where f (Ty TyConstant) j =
                                CEq (Ty TyContext) j 
                            f i j = CEq i j 
                  Just _ ->
                      error $ "Number of arguments mismatch: call of " ++ show f 
                  _ -> 
                      error $ "No such function or variable: " ++ show f 
      gatherE fs env (ESeq _ e1 e2) =
          do { (c1,t1) <- gatherE fs env e1 
             ; (c2,t2) <- gatherE fs env e2 
             ; return ( CEq t1 (Ty TyDocument): CEq t2 (Ty TyDocument) : c1 ++ c2, 
                        Ty TyDocument ) }
      gatherE fs env (ELine _) =
          do { return ( [], Ty TyDocument ) }
      gatherE fs env (EGroup _ e) = 
          do { (c,t) <- gatherE fs env e 
             ; return ( CEq t (Ty TyDocument):c, 
                        Ty TyDocument ) }
      gatherE fs env (ENest _ _ e) =
          do { (c,t) <- gatherE fs env e 
             ; return ( CEq t (Ty TyDocument):c,
                        Ty TyDocument ) }
      gatherE fs env (EText _ e) =
          do { (c,t) <- gatherE fs env e 
             ; return ( f t c,
                        Ty TyDocument ) }
          where
            f (Ty TyConstant) c = c
            f (Ty TyContext)  c = c
            f j               c = CEq j (Ty TyContext): c 
      gatherE fs env (EAltL _ e1 e2) =
          do { (c1,t1) <- gatherE fs env e1 
             ; (c2,t2) <- gatherE fs env e2 
             ; return ( CEq t1 (Ty TyDocument): CEq t2 (Ty TyDocument) : c1 ++ c2, 
                        Ty TyDocument ) }
      gatherE fs env (EReify _ e1 e2) =
          do { (c1,t1) <- gatherE fs env e1 
             ; (c2,t2) <- gatherE fs env e2 
             ; return ( CEq t1 (Ty TyReg):CEq t2 (Ty TyInput) : c1++c2, 
                        Ty TyDocument ) }
      gatherE fs env (ECharOf _ e) = 
          do { (c,t) <- gatherE fs env e -- variables occurring in e must be of TyContext 
             ; return (CEq t (Ty TyCharSet):c, Ty TyDocument)}
      
      gatherE fs env (EBij _ n e) = 
          do { (c,t) <- gatherE fs env e 
             ; return ( CEq t (Ty TyInput): c, Ty TyInput) }

      gatherE fs env (EInt _ i) =
          return ( [], Ty TyConstant ) 
      gatherE fs env (EString _ str) =
          return ( [], Ty TyConstant ) 
                           
      gatherE fs env (ECon _ _ es) =
          do { (cs,ts) <- fmap unzip $ mapM (gatherE fs env) es 
             ; return $ ( zipWith CEq ts (cycle [Ty TyConstant]) ++ concat cs, 
                          Ty TyConstant ) }

      gatherE fs env (EIf _ e1 e2 e3) =
          do { (c1,t1) <- gatherE fs env e1
             ; (c2,t2) <- gatherE fs env e2 
             ; (c3,t3) <- gatherE fs env e3 
             ; return $ ( CEq t1 (Ty TyContext) : CEq t2 t3 : c1 ++ c2 ++ c3, 
                          t2 ) }


      gatherE fs env (EChar _ c) = return ([],Ty TyConstant)
      -- gatherE fs env (EPrim _ (Name n) es) =
      --     do { (cs,ts) <- fmap unzip $ mapM (gatherE fs env) es 
      --        ; let ty_u  = ([Ty TyContext],Ty TyContext) 
      --        ; let ty_b  = ([Ty TyContext,Ty TyContext],Ty TyContext) 
      --        ; let cs_b  = ([Ty TyCharSet,Ty TyCharSet],Ty TyCharSet)
      --        ; let cs_is = ([Ty TyContext],Ty TyCharSet)
      --        ; let cs_null = ([], Ty TyCharSet)
      --        ; let (is,i) =
      --                  case n of 
      --                    "=="  -> ty_b 
      --                    "/="  -> ty_b 
      --                    "<="  -> ty_b 
      --                    ">="  -> ty_b 
      --                    "<"   -> ty_b 
      --                    ">"   -> ty_b
      --                    "&&"  -> ty_b 
      --                    "||"  -> ty_b 
      --                    "not" -> ty_u 
      --                    "is"  -> cs_is 
      --                    "andalso" -> cs_b 
      --                    "orelse"  -> cs_b 
      --                    "butnot"  -> cs_b 
      --                    "anyChar" -> cs_null 
      --                    "none"    -> cs_null 
      --                    _     -> error $ "Unknown primitive function: " ++ n 
      --          ; return ( zipWith f ts is ++ concat cs, 
      --                             i )
      --        }
      --     where f (Ty TyConstant) j =
      --               CEq (Ty TyContext) j 
      --           f i j = CEq i j 

      -- gatherCE env (CEVar _ x) =
      --     case lookup x of
      --       Just t ->
      --           [CEq t (Ty TyContext)]
      --       Nothing ->
      --           error $ "No such a function or variable: " ++ show x
      -- gatherCE env e = 
      --     composFold (gatherCE env) e 

                 
    

type Substitution = [(Int, Ty)]

(>>>>) s1 s2 =
    map (\(i,d) -> 
             case d of 
               Ty d -> (i,Ty d) 
               TyVar j ->
                   case lookup j s2 of 
                     Just x  -> (i,x) 
                     Nothing -> (i,TyVar j)) s1 
    ++ [ (i,d) | (i,d) <- s2, not (elem i $ map fst s1) ]


solveConstraint :: [Constraint] -> Substitution                         
solveConstraint constraints =
    if checkCp otherCs then 
        subst 
    else 
        undefined -- This code cannot be executed 
    where
      subst = unify eqCs 
      
      (eqCs, otherCs) =
          ([ CEq t1 t2  | CEq t1 t2 <- constraints],
           applyCs subst [ Cp t | Cp   t <- constraints ])
      
      checkCp [] = True
      checkCp (Cp (Ty TyConstant):cs) = error "Constants cannot be pattern matched"
      checkCp (Cp (Ty TyDocument):cs) = error "Doc cannot be pattern matched"
      checkCp (Cp _:cs)               = checkCp cs 

      unify [] = []
      unify (CEq (TyVar i) t2:cs) = 
          [(i,t2)] >>>> unify (applyCs [(i,t2)] cs)
      unify (CEq (Ty d) (Ty d'):cs) | d == d' = unify cs 
                                   | d /= d' = error $ "Type Error: " ++ show d ++ "/=" ++ show d'
      unify (CEq t1 t2:cs) = unify (CEq t2 t1:cs)

      applyCs sub [] = []
      applyCs sub (CEq t1 t2:cs) =
          CEq (applyTy sub t1) (applyTy sub t2):applyCs sub cs 
      applyCs sub (Cp t:cs) =
          Cp (applyTy sub t):applyCs sub cs 

applyTy sub (Ty d) = Ty d 
applyTy sub (TyVar i) =
    case lookup i sub of 
      Just d  -> d 
      Nothing -> TyVar i
          
typing :: Prog a -> [(Name,[Type],Type)]
typing prog =
    let (tmpl,constraint) = gatherContraint prog 
        subst             = solveConstraint constraint
    in applyTmpl subst tmpl
    where
      applyTmpl subst tmpl = map f tmpl
          where f (n,is,i) = (n, map (unTy . applyTy subst) is, unTy $ applyTy subst i )
                unTy (TyVar i) = TyInput 
                unTy (Ty d)    = d 
