module RegexParser (parseRE, parseClass) where 

import Control.Applicative hiding (many,some)
import Data.Char 

import RegularGrammar hiding (many)
import Text.TDParser 
import Data.CharSet (CharSet)
import qualified Data.CharSet as CS
import Data.Monoid 

parseRE    = either error id . runParser pRE 
parseClass = either error id . runParser pClass

many k p = memoize k $ 
           [] <$ text ""
           <|>
           (:) <$> p <*> many k p 

manyP k p = (:) <$> p <*> many k p 

digit = foldl (\a b -> a*10 + f b) 0 <$> 
          manyP (-1) (charOf CS.digitChars)
  where
    f b = ord b - ord '0'
            
hexdigit = foldl (\a b -> a*16 + f b) 0 <$>
              manyP (-2) (charOf CS.digitChars)
  where
    f b | '0' <= b && b <= '9' = ord b - ord '0' 
    f b | 'a' <= b && b <= 'f' = ord b - ord 'a' + 10
    f b | 'F' <= b && b <= 'F' = ord b - ord 'F' + 10 
            
                                 
sClass = text "\\" *> f
  where
    f = CS.digitChars <$ text "d"
        <|> (CS.neg CS.digitChars) <$ text "D"
        <|> CS.spaceChars <$ text "s"
        <|> (CS.neg CS.spaceChars) <$ text "s"
  
pClass = foldr (CS.union) (CS.empty) <$> many 0 f
  where 
    f = range <|> sClass <|> singleton 
    range = 
      (\s e -> CS.charSetRange s e) <$> 
       ch <* text "-" <*> ch 
    singleton =
      CS.singleton <$> ch      
    ch = char "[]-\\"
        
char esc = 
  (text "\\" *> (escaped <|> number <|> control))
  <|> normal
  where
    escaped = charOf (CS.fromList esc)
    normal  = charOf (CS.neg $ CS.fromList esc)
    number  = chr <$> ((text "0x" *> hexdigit)
                           <|> digit) 
    control = 
          '\n' <$ text "n" 
      <|> '\v' <$ text "v"
      <|> '\t' <$ text "t"
      <|> '\r' <$ text "r"
      <|> '\a' <$ text "a"
      <|> '\b' <$ text "b"
      <|> '\f' <$ text "f"
      
      
pRE = memoize 1 f       
  where
    f = ReOr <$> pRE <* text "|" <*> pSeq 
        <|> ReAnd <$> pRE <* text "&" <*> pSeq
        <|> (\x y -> ReAnd x (ReNot y)) <$> pRE <* text "#" <*> pSeq 
        <|> pSeq 
        
pSeq = memoize 2 f         
  where
    f = ReThen <$> pAtom <*> pSeq 
        <|> ReEps <$ text ""
        <|> pAtom 
        
pAtom = memoize 3 f         
  where 
    f = ReCharSet <$ text "[^" <*> pClass <* text "]"
        <|> ReCharSet <$ text "[" <*> pClass <* text "]"
        <|> ReRep <$> pAtom <* text "*"
        <|> (\r -> r <> ReRep r) <$> pAtom <* text "+"
        <|> (\r -> ReOr r ReEps) <$> pAtom <* text "?"
        <|> ReCharSet CS.universal <$ text "." 
        <|> ReCharSet <$> sClass 
        <|> (ReCharSet . CS.singleton) <$>
              char "\\-[]()+?*.|&#"
        <|> text "(" *> pRE <* text ")"