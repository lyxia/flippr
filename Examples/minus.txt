-- data E = Minus E E | Div E E | One 
--

pprMain x = nil <> ppr 5 x <> nil;
ppr  i x = manyParens (ppr' i x);
ppr' i One         = text "1" ;
ppr' i (Minus x y) = 
      parensIf (i >= 6) $ group $ 
            ppr 5 x <> nest 2 (line' <> text "-" <> space' <> ppr 6 y);
ppr' i (Div   x y) = 
      parensIf (i >= 7) $ group $
            ppr 6 x <> nest 2 (line' <> text "/" <> space' <> ppr 7 y) ;

parensIf b x = if b then parens x else x;

manyParens d = d <+ parens (manyParens d);
parens d = text "(" <> nest 1 (nil <> d <> nil) <> text ")";
space = (text " " <+ charOf nospace) <> nil;
nil = text "" <+ (charOf spaceChars <> nil);

nospace = spaceChars `butnot` chars " ";

line'  = line <+ text "" ; 
space' = space <+ text "";

