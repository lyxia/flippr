{-# OPTIONS_GHC -XDeriveDataTypeable -XRank2Types -XGADTs #-}
module Examples.Defs where 

import Data.Typeable 
import qualified Data.Set as S

import Util

data Bin = Lf | Nd Bin Bin deriving (Show, Typeable, Eq)


data Dir = L | R deriving Eq 
data E = Int Int
       | Var String
       | Minus E E
       | Div E E
       | Add E E
       | IfZ E E E
       | One
       | Sub E E 
       | Zero deriving (Show, Typeable, Eq) 

i2a :: Bij Int String 
i2a = Bij (show :: Int -> String) (read :: String -> Int)

data Ex a = ExVar a String 
          | ExInt a Int 
          | ExLet a String (Ex a) (Ex a) 
          | ExSub a (Ex a) (Ex a)
            deriving (Show, Typeable, Eq)
                     
                     
type Prog = [Rule]                     
data Rule = Rule String [Pat] Exp deriving (Show,Typeable,Eq)
data Exp  = ECon String [Exp] | EOp Op Exp Exp | EVar String [Exp]  deriving (Show,Typeable,Eq)
data Pat  = PVar String | PCon String [Pat] deriving (Show,Typeable,Eq)
data Op   = OCat | OAlt  deriving (Show,Typeable,Eq)

prog = [ Rule "snoc" [PCon "Nil" [], PVar "b"] 
               (ECon "Cons" [EVar "b" [], ECon "Nil" []]),
         Rule "snoc" [PCon "Cons" [PVar "a", PVar "x"], PVar "b"] 
               (ECon "Cons" [EVar "b" [], EVar "snoc" [EVar "x" [], EVar "a" []]])] 