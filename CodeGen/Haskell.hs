module CodeGen.Haskell where 

-- Code Generator for Haskell 

import Text.WadlerPpr 

import Data.List  (elemIndex)
import Data.Maybe 

import qualified Data.Set as S 

import Data.Graph 

import Name 
import Syntax
import ParserSyntax 
import qualified Data.CharSet as CS

import Data.Composable 
import Debug.Trace 

import Inverter

codeGen :: [String] -> Prog a -> (Name,ParseProg) -> String 
codeGen imps ppr parser = 
    parseStub ++ "\n\n" ++ unlines imps ++ "\n\n" ++ codeGenPprBase ppr ++ "\n\n" ++ codeGenParseBase parser
    -- where
    --   stubCode = 
    --       unlines ["import Control.Applicative",
    --                "import Data.Dynamic",
    --                "import Data.Maybe (fromJust)", 
    --                "import Data.Char",
    --                "",
    --                "import WadlerPpr hiding (parens, brackets, nil)", 
    --                "import qualified WadlerPpr as W",
    --                "import TDParser hiding (text, charOf, parsedString)", 
    --                "import qualified TDParser as T (text, charOf, parsedString)", 
    --                "import qualified CharSet as  CS (member, CharSet(..))",
    --                "import RegularGrammar (re, intersect, union, (\\\\))",
    --                "",
    --                "import Util (charOf, (@@), (<+), bij, andalso, orelse, butnot, is, anyChar, none)",
    --                "import qualified Util as U"]

    
pprStub = unlines      
      [ "import Data.Char",
        "",
        "import WadlerPpr hiding (parens, brackets, nil)", 
        "import qualified WadlerPpr as W",
        "import Util (charOf, (@@), (<+), bij, andalso, orelse, butnot, is, anyChar, none)",
        "import qualified Util as U"]
      
parseStub = pprStub ++ unlines     
      [ "import Control.Applicative",
        "import Data.Dynamic",
        "import Data.Maybe (fromJust)", 
        "",
        "import TDParser hiding (text, charOf, parsedString)", 
        "import qualified TDParser as T (text, charOf, parsedString)", 
        "import qualified CharSet as CS"
      ]
                   
      
codeGenPpr :: [String] -> Prog a -> String 
codeGenPpr imps ppr =
  pprStub ++ "\n\n" ++ unlines imps ++ "\n\n" ++ codeGenPprBase ppr 
      
codeGenParse :: [String] -> (Name, ParseProg) -> String       
codeGenParse imps parser =      
  parseStub ++ "\n\n" ++ unlines imps ++ "\n\n" ++  codeGenParseBase parser


codeGenPprBase :: Prog a -> String 
codeGenPprBase prog_ = 
    pretty 80 $ pprProg (filter isReachableD prog)
    where
      prog = conv prog_
      
      conv :: Prog a -> Prog a 
      conv = map (\ (Decl i n ps e) -> Decl i n ps (f e))
        where
          f (EAltL _ e _)  = f e 
          f (EReify i _ e) = EText i (f e)
          f (ECharOf i e)  = let CS.CharSet cs = evalCE e 
                             in case cs of 
                               []  -> EVar i (Name "undefined") []
                               c:_ -> EText i (EString i [c])
          f e = composOp f e 
          
      
      main = head $ [ f | Decl _ f _ _ <- prog ]
      
      (callGraph, v2n, k2v) =
        graphFromEdges $ 
          [ (f, f, S.toList $ S.fromList $ concat [ freeVarsRHS d | d@(Decl _ g _ _) <- prog, g == f ])
            | f <- functions ]
        where
          functions =  S.toList $ S.fromList [ f | Decl _ f _ _ <- prog ]
      
      isReachableD (Decl _ f _ _) = isReachable f 

      isReachable g =
        case k2v g of
          Just v -> 
            path callGraph (fromJust $ k2v main) v 
          Nothing -> 
            False

    -- where
    --   ppr ds = foldDoc (\x y -> x <> text "\n\n" <> y) (map pprD ds) 
      -- pprD (Decl _ f ps e) = 
      --     group (text (show f) <> nest 4 (line <> foldDoc (</>) (map pprPat ps) <+> text "=")) <>
      --       nest 4 (line <> pprExp 0 e)

      -- precAlt = 5
      -- precSeq = 6 
      -- precApp = 9      

      -- pprPat (PVar _ x) = text (show x)
      -- pprPat (PCon _ (Name "Cons") [x,y]) = parens (pprPat x <> text ":" <> pprPat y)
      -- pprPat (PCon _ (Name "Nil")  [])    = text "[]"
      -- pprPat (PCon _ c ps)         = parens (text (show c) <+> 
      --                                           foldDoc (<+>) (map pprPat ps))

      -- parensWhen b d = if b then parens d else d 

      -- asBin op [x1,x2] = 
      --     parens $ pprExp precApp x1 <+> text op <+> pprExp precApp x2 

      -- pprExp i (EVar _ f [])   = text (show f) 
      -- pprExp i (EVar _ f xs)   = group $ parensWhen (i >= precApp) $ text (show f) <+> foldDoc (<+>) (map (pprExp precApp) xs)
      -- pprExp i (ECon _ c [])   = text (show c) 
      -- pprExp i (ECon _ c xs)   = group $ parensWhen (i >= precApp) $ text (show c) <+> foldDoc (<+>) (map (pprExp precApp) xs)
      -- pprExp i (EIf _ e1 e2 e3)= group $ text "if" <+> pprExp 0 e1 <+> text "then" 
      --                                   <> nest 4 (line <> pprExp 1 e2)
      --                                   <> line <> text "else" 
      --                                   <> nest 4 (line <> pprExp 0 e3)
      -- pprExp i (EPrim _ f [])   = text (show f) 
      -- pprExp i (EPrim _ (Name f) xs)   = 
      --     case f of 
      --       "==" -> asBin "==" xs 
      --       "/=" -> asBin "==" xs 
      --       "<=" -> asBin "<=" xs 
      --       ">=" -> asBin ">=" xs
      --       "<"  -> asBin "<" xs 
      --       ">"  -> asBin ">" xs 
      --       _ ->
      --           group $ parensWhen (i >= precApp) $ text f <+> foldDoc (<+>) (map (pprExp precApp) xs)
      -- pprExp i (ESeq _ e1 e2)  = group $ parensWhen (i >= precSeq) $ 
      --                             pprExp (precSeq-1) e1 </> text "<>" <+> nest 3 (pprExp (precSeq-1) e2)
      -- pprExp i (ELine _)       = text "line"
      -- pprExp i (EGroup _ e)    = parensWhen (i >= precApp) $ group $ 
      --                           text "group" </> nest 2 (pprExp precApp e)
      -- pprExp i (ENest _ e1 e2) = parensWhen (i >= precApp) $ 
      --                            group (text "nest" <+> pprExp precApp e1 </> nest 2 (pprExp precApp e2))

      -- pprExp i (EText _ e)     = parensWhen (i >= precApp) $ 
      --                            text "text" <+> pprExp precApp e
      -- pprExp i (EAltL _ e1 e2) = parensWhen (i >= precAlt) $ group $ 
      --                            pprExp (precAlt-1) e1 </> text "<+" </> pprExp (precAlt-1) e2 
                                        
      -- pprExp i (EReify _ e1 e2) = parensWhen (i >= precAlt) $ group $ 
      --                            text "reify" <+> pprExp precApp e1 </> nest 2 (pprExp precApp e2)

      -- pprExp i (ECharOf _ n)    = parensWhen (i >= precApp) $ 
      --                               text "charOf" <+> text (show n) 

      -- pprExp i (EInt _ j)       = text (show j)
      -- pprExp i (EString _ s)    = text (show s)

-- More "sophisticated approach"
codeGenParseBase :: (Name,ParseProg) -> String
codeGenParseBase (mainFunc, prog) =
    pretty 80 mainCode ++ "\n\n"++ (pretty 80 $ ppr prog)
    where
      functions = [ f | ParseDecl f _ <- prog ]
      n2i f = fromJust $ elemIndex f functions 

      encName f = text ("p_" ++ show f)

      doesContainPH (PPPlaceHolder) = True
      doesContainPH (PPVar _)       = False
      doesContainPH (PPCon _ ps)    = any doesContainPH ps

      mainCode = 
          text ("parse_" ++ show mainFunc) <+> text "str" <+> text "=" 
               <> nest 4 (line <> text "runParser " <> encName mainFunc <> text " str")

      ppr ds = foldDoc (\x y -> x <> text "\n\n" <> y) (map pprD ds)

      pprD (ParseDecl f p) = 
          encName f <> nest 2 (line <> text "=" <+> text "memoize" <+> text (show $ n2i f) <+> text "$") <>
              nest 4 (line <> pprP p)
          
      freeVars = S.toList . S.fromList . go 
          where
            go (ParseFmap f p)  = goF f ++ concatMap go p
            go (ParseAlt p1 p2) = go p1 ++ go p2 
            go (ParsedString p) = go p -- this must result in empty list 
            go _                = []
            goF (FAssignS n)  = [n]
            goF (FAssignF ns) = ns
            goF (FReconst ps) = []  -- this code is not reachable 
            goF _             = []
            
            
      pprP (ParseEmpty)     = text "empty"
      pprP (ParseText s)    = text $ "T.text " ++ (show s) 
      pprP (ParseCharOf cs) =
          text "T.charOf" <+> f cs
        where
          f c@(CS.CharSet cs)
            | c == CS.spaceChars = text "CS.spaceChars" 
            | c == CS.alphaChars = text "CS.alphaChars" 
            | otherwise = 
              parens (text "CS.CharSet" <+> text (show cs) <+> 
                      text "{-" <+> text (show (CS.CharSet cs)) <+> text "-}" )
      pprP (ParsedString p) =
          text "T.parsedString" <+> pprP p
      pprP (ParseNTerm f)   = encName f 
      pprP (ParseAlt p1 p2) = group (pprP p1 </> text "<|>" </> pprP p2)
      pprP (ParseFmap f ps) = pprF f ps 

      pprP1 p@(ParseAlt p1 p2) = parens $ pprP p 
      pprP1 p                  = pprP p 

      tuple [p] = p 
      tuple ps  = parens $ foldDoc com ps 
          where
            com x y = x <> text "," <> y 

      tupleName ns = tuple $ map pprName ns 

      pprName = text . show 

      snub = S.toList . S.fromList 

      pprF FAppend [p1,p2] = group $ 
          parens $ parens (text "\\" <> tupleName (freeVars p1)
                              <+> tupleName (freeVars p2) <+> text "->" 
                            <> tupleName (snub $ freeVars p1 ++ freeVars p2)) <+> text "<$>"
                   <> nest 4 (line <> pprP1 p1 <+> text "<*>" <+> pprP1 p2)

      pprF FIgnore [p] = 
          group $ parens $ 
                text "U.ignoreU" <+> text "<$>" <> nest 4 (line <> pprP1 p)

      pprF SEmpty  [p] = 
          group $ parens $
                text "(\\_ -> [])" <+> text "<$>" <> nest 4 (line <> pprP1 p)

      pprF SAppend [p1,p2] = 
          group $ parens $ 
                text "(++)" <+> text "<$>" <> nest 4 (line <> pprP1 p1 <+> text "<*>" <+> pprP1 p2)

      pprF SSingle [p] = 
          group $ parens $
                text "(:[])" <+> text "<$>" <> nest 4 (line <> pprP1 p)           

      pprF (FAssignS n)  [p] = parens $ pprP p 

      pprF (FAssignF ns) [p] = parens $ pprP p 

      pprF (FBij f) [p] = group $ parens $ 
          let fvs = freeVars p 
          in case fvs of 
               [v] -> 
                   text "U.inv" <+> pprName f <+> text "<$>" <> nest 4 (line <> pprP1 p)
               _ -> 
                   parens (text "\\" <> tupleName fvs <+> text "->" <>
                          tuple (map (\x -> text "U.inv" <+> pprName f <+> pprName x) fvs)) <+> text "<$>" 
                   <> nest 4 (line <> pprP1 p)
      pprF (FReconst ps) [p]
          | any doesContainPH ps =
              parens (text "\\" <> sp <+> tupleName (freeVars p) <+> ep <+> text "->"
                     <> nest 4 (line <> tuple (map pprReconst ps))) <+> text "<$>"
              <> nest 4 ( line <> text "currentPosition" <+> 
                          line <> text "<*>" <+> pprP1 p <+>
                          line <> text "<*> currentPosition")
          | otherwise =
              parens (text "\\" <> tupleName (freeVars p) <+> text "->" 
                      <> nest 4 (line <> tuple (map pprReconst ps))) <+> text "<$>"
              <> nest 4 (line <> pprP1 p)

      sp = text "__startPos"
      ep = text "__endPos" 

      pprReconst p = conv p 
          where
            conv (PPVar x) = pprName x 
            conv (PPPlaceHolder) = parens $ sp <> text "," <> ep 
            conv (PPCon (Name "Nil") []) = text "[]"
            conv (PPCon c []) = text (show c)
            conv (PPCon (Name "Cons") [x,y]) =
                parens $ conv x <+> text ":" <+> conv y 
            conv (PPCon c ps) = parens $ text (show c) <+> (foldDoc (<+>) $ map conv ps)
      
          
-- pprChE i (PChPrim f []) = text (show f) 
-- pprChE i (PChPrim (Name "andalso") [x,y]) 
--     = parensWhen (i >= 2) $
--        pprChE 1 x <+> text "`andalso`" <+> pprChE 2 y 
-- pprChE i (PChPrim (Name "orelse") [x,y]) 
--     = parensWhen (i >= 2) $
--        pprChE 1 x <+> text "`orelse`" <+> pprChE 2 y 
-- pprChE i (PChPrim (Name "butnot") [x,y]) 
--     = parensWhen (i >= 2) $
--        pprChE 1 x <+> text "`butnot`" <+> pprChE 2 y       
-- pprChE i (PChIs c)   = parensWhen (i >= 3) $ text "is" <+> text (show c)
-- pprChE i (PChIn (CS.CharSet cs)) = 
--     parensWhen (i >= 3) $ text "flip CS.member" <+>
--          parens (text "CS.CharSet" <+> text (show cs) <+> text "{-" <+> text (show (CS.CharSet cs)) <+> text "-}" )
-- pprChE i p = error $ show p


-- codeGenParse' :: (Name, ParseProg) -> String 
-- codeGenParse' (mainFunc,prog) = 
--     pretty 80 mainCode ++ "\n\n"++ (pretty 80 $ ppr prog)
--     where
--       functions = [ f | ParseDecl f _ <- prog ]
--       n2i f = fromJust $ elemIndex f functions 

--       mainCode = 
--           text ("parse_"++show mainFunc) <+> text "str" <+> text "=" <> nest 4 (line <> 
--               text ("let x = (\\_ -> undefined) ("++show mainFunc++" x)") 
--                </> text "in fmap (map (flip fromDyn x)) "
--                <> text "(runParser " <> encName mainFunc <> text " str)")

--       encName f = text ("p_" ++ show f)

--       doesContainPH (PPPlaceHolder) = True
--       doesContainPH (PPVar _)       = False
--       doesContainPH (PPCon _ ps)    = any doesContainPH ps

--       ppr ds = foldDoc (\x y -> x <> text "\n\n" <> y) (map pprD ds)
--       pprD (ParseDecl f p) = 
--           encName f <+> text "::" <+> text "Parser m Int [Dynamic]" </>
--           encName f <> nest 2 (line <> text "=" <+> text "memoize" <+> text (show $ n2i f) <+> text "$") <>
--               nest 4 (line <> pprP p)
                   
--       pprP (ParseEmpty)     = text "empty"
--       pprP (ParseText s)    = text $ "T.text " ++ (show s) 
--       pprP (ParseNTerm f)   = encName f 

--       pprP (ParseAlt p1 p2) = group (pprP p1 </> text "<|>" </> pprP p2)

--       pprP (ParseFmap f@(FReconst ps) p) 
--           | any doesContainPH ps = 
--               parens $ group $
--                 group( text "(\\startPos e endPos -> " <>
--                          nest 4 (line <> pprF f <+> text "e)" )) <+> 
--                   text "<$>" <> 
--                      nest 4 ( line <> text "currentPosition" <+> 
--                               line <> text "<*>" <+> nest 4 (group $ parens $ text "id <$>" <+> pprSeq p) <+>
--                               line <> text "<*> currentPosition")
--       pprP (ParseFmap f p ) =
--           parens $ group (pprF f <+> text "<$>" <> nest 4 (line <> pprSeq p))
--       pprP (ParseCharOf n) =
--           text "T.charOf" <+> pprChE 3 n
--       pprP (ParsedString p) =
--           text "T.parsedString" <+> pprP p


--       pprP1 p@(ParseAlt p1 p2) = parens $ pprP p 
--       pprP1 p                  = pprP p 

--       pprSeq [x]   = pprP1 x 
--       pprSeq (x:y) = group $ pprP1 x </> text "<*>" <+> nest 4 (pprSeq y)


--       pprF (FBij n)      = text "U.invMap" <+> text (show n)
--       pprF (FAppend)     = text "(++)"
--       pprF (FAssignS x)  = text "U.assign"  <+> quotes (text (show x))
--       pprF (FAssignF xs) = text "U.assignS" <+> brackets (foldDoc withComma $ map (quotes . text . show) xs)

--       pprF (FIgnore)     = text "U.ignore" 

--       pprF (FReconst ps) = parens $ group $ text "\\env ->" <> nest 4 (line <> brackets (foldDoc withComma $ map conv ps))
--           where
--             conv (PPVar x) = parens $ text "fromJust $ lookup " <> quotes (text (show x)) <> text "env"
--             conv (PPPlaceHolder) = text "(startPos,endPos)"
--             conv p = text "toDyn" <+> go p
--             go (PPVar x)    = parens $ text "U.resolve env" <+> (quotes (text (show x)))
--             go (PPCon c []) = text (show c) 
--             go (PPCon c ps) = parens $ text (show c) <+> (foldDoc (<+>) $ map go ps)
--             go (PPPlaceHolder) = text "(startPos,endPos)"

                           

--       quotes x = text "\"" <> x <> text "\""

--       withComma x y = x <> text "," <> y 



            

      


