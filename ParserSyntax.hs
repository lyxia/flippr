{-|
This module defines the datatypes for AST of 
parser-descriptions (CFG with actions), i.e., the outputs of FliPpr.  

-}
module ParserSyntax 
    (
     ParseProg, 
     ParseDecl(..), 
     ParseExp(..), 
     FExp(..),
     PPat(..),
    ) where

import Name 
import Data.CharSet 

type ParseProg = [ ParseDecl ]
data ParseDecl = ParseDecl Name ParseExp 

-- | AST for parser, suitable for |Applicative| parsers.
data ParseExp = ParseFmap   FExp     [ParseExp] -- f <$> e1 <*> e2 <*> .. <*> en 
              | ParseAlt    ParseExp ParseExp   -- e1 <|> e2 
              | ParseText   String              
--              | ParseCharOf PChExp               -- charOf n. n must be of Char -> Bool
              | ParseCharOf CharSet 
              | ParsedString ParseExp           -- retriving a parsed string 
              | ParseEmpty                      -- empty (always fail)
              | ParseNTerm  Name                

-- | Semantic actions. 
data FExp = FIgnore         -- \_ -> emptyEnv 
          | FAppend         -- \env1 env2 -> env1 ++ env2
          | FAssignS Name   -- FAssignS x  = \s  -> [(x,s)]
          | FAssignF [Name] -- FAssignF ns = \vs -> zip ns vs
          | FReconst [PPat] -- \env -> ps [ env ] 
          | FBij     Name   -- bijection application 
          | SEmpty  -- \_ -> []
          | SAppend -- (++)
          | SSingle -- :[]

-- | Patterns used for construction of data.
data PPat = PPVar Name
          | PPCon Name [PPat]
          | PPPlaceHolder     -- PlaceHolder of "position information"
            deriving Show 

-- -- | We assume that a parser supports character classes. 
-- data PChExp = PChPrim Name [PChExp]
--             | PChIs        Char
--             | PChIn        CharSet 
--               deriving Show 


