{-|
This module provides datatypes for regular expressions/grammars 
and options on them.
-}

{-# LANGUAGE DeriveFunctor, DeriveDataTypeable  #-}

module RegularGrammar where 

import Data.List (group, sort)
import qualified Data.List as L 
import Data.Monoid 

import Data.Graph 
import qualified Data.Map as M 
import qualified Data.Set as S 

import Control.Monad.State
import Data.Maybe 

import Debug.Trace
import Data.Typeable 
import Control.Applicative hiding (many, empty)

import Data.CharSet (CharSet) 
import qualified Data.CharSet as CS
import qualified ParserSyntax as PS

import Text.TDParser
import Name 

-- | Datatype for regular expressions. 
data Reg = ReCharSet CharSet 
         | ReThen Reg Reg          -- ^ "followed by" Relation
         | ReOr   Reg Reg          -- ^ Choice
         | ReRep  Reg              -- ^ Kleene Star
         | ReEps                   -- ^ Empty Sequence (unit of ReThen)
         | ReNull                  -- ^ Empty Set (unit of ReOr)
         | ReAnd  Reg Reg          -- ^ Intersection
         | ReNot  Reg              -- ^ Complement
           deriving (Typeable, Show)

instance Monoid Reg where
    mempty  = ReEps 
    mappend = ReThen 

(\\) :: Reg -> Reg -> Reg 
(\\) x y = ReAnd x (ReNot y)

many :: Reg -> Reg
many = ReRep 

union :: Reg -> Reg -> Reg 
union = ReOr 

intersect :: Reg -> Reg -> Reg 
intersect = ReAnd 

empty :: Reg 
empty = ReNull 

nil :: Reg 
nil = ReEps 

eitherOf :: [Reg] -> Reg 
eitherOf = foldr union empty 

rchar :: Char -> Reg 
rchar c = ReCharSet (CS.singleton c)

rtext :: String -> Reg
rtext = foldr (\a r -> rchar a <> r) ReEps 


-- -- | The function |re| parses regular expresions. 
-- re :: String -> Reg 
-- re str = 
--     case runParser parseRE str of
--       Left s  -> error s 
--       Right r -> r 
--     where
--       parseRE = memoize 0 $
--                       (\x _ y -> ReOr x y)  <$> parseRE <*> text "|" <*> parseSeq
--                   <|> (\x _ y -> ReAnd x y) <$> parseRE <*> text "&" <*> parseSeq 
--                   <|> (\x _ y -> ReAnd x (ReNot y)) <$> parseRE <*> text "#" <*> parseSeq
--                   <|> parseSeq 

--       parseSeq = memoize 1 $
--                      ReThen <$> parseAtom <*> parseSeq 
--                  <|> (\_ -> ReEps) <$> text ""


--       parseAtom = memoize 2 $
--                       (\_ c _ -> ReCharSet (CS.neg c)) <$> 
--                          text "[^" <*> parseClass <*> text "]" 
--                   <|> (\_ c _ -> ReCharSet c) <$> 
--                          text "[" <*> parseClass <*> text "]"
--                   <|> (\r _ -> ReRep r) <$> parseAtom <*> text "*"
--                   <|> (\r _ -> r <> ReRep r) <$> parseAtom <*> text "+"
--                   <|> (\r _ -> ReOr r ReEps) <$> parseAtom <*> text "?"
--                   <|> (\_ c -> ReCharSet (CS.singleton c)) <$> text "\\" <*> specialChar 
--                   <|> (\_ -> ReCharSet CS.universal) <$>
--                          text "."
--                   <|> (\c -> ReCharSet (CS.singleton c)) <$> normalChar 
--                   <|> (\_ r _ -> r) <$>
--                          text "(" <*> parseRE <*> text ")"
--           where
--             specialSymbols = "(|)[].+*?\\&#"
--             specialChar = charOf (\x -> x `elem` specialSymbols)
--             normalChar = charOf (\x -> x `notElem` specialSymbols)

--       parseClass = memoize 3 $
--                        (\_ c cs' ->  CS.singleton c `CS.union` cs') <$>
--                          text "\\" <*> specialChar <*> parseClass 
--                    <|> (\s _ e cs' -> CS.charSetRange s e `CS.union` cs') <$>
--                          normalChar <*> text "-" <*> normalChar <*> parseClass
--                    <|> (\c cs' -> CS.singleton c `CS.union` cs') <$>
--                          normalChar <*> parseClass 
--                    <|> (\_ -> CS.empty ) <$> text "" 
--           where
--             specialSymbols = "[]\\-"
--             specialChar = charOf (\x -> x `elem` specialSymbols)
--             normalChar  = charOf (\x -> x `notElem` specialSymbols)
                                               
        
-- | Transition-to in automata.                          
data Trans a = Eps a 
             | Lab CharSet a 
               deriving (Show,Eq,Ord)

instance Functor Trans where 
    fmap f (Eps a)   = Eps (f a)
    fmap f (Lab c a) = Lab c (f a)

-- destination 
dst (Eps a)   = a 
dst (Lab _ a) = a 

-- data CharSet = Pos (S.Set Char)
--              | Neg (S.Set Char)
--              deriving (Show, Ord, Eq)

-- intersectSet (Pos cs) (Pos cs') = Pos $ S.intersection cs cs' 
-- intersectSet (Pos cs) (Neg cs') = Pos $ (S.\\) cs cs' 
-- intersectSet (Neg cs) (Pos cs') = Pos $ (S.\\) cs' cs 
-- intersectSet (Neg cs) (Neg cs') = Neg $ S.union cs cs' 

-- unionSet (Pos cs) (Pos cs') = Pos $ S.union cs cs'
-- unionSet (Pos cs) (Neg cs') = Neg $ (S.\\) cs' cs 
-- unionSet (Neg cs) (Pos cs') = Neg $ (S.\\) cs  cs'
-- unionSet (Neg cs) (Neg cs') = Neg $ S.intersection cs cs' 

-- negSet (Pos cs) = Neg cs 
-- negSet (Neg cs) = Pos cs 

-- isEmptySet (Pos cs) = S.null cs 
-- isEmptySet (Neg cs) = S.size cs == numberOfAllChars
--     where
--       numberOfAllChars = fromEnum (maxBound :: Char) - fromEnum (minBound :: Char)

-- singletonSet s = Pos $ S.singleton s 
-- emptySet     = Pos S.empty 
-- universalSet = Neg S.empty 

-- | Finite-state Automata 
data Automaton a = A { initial :: a
                     , finals  :: [a]
                     , transitions ::[(a, Trans a)] }
                 deriving Functor 

instance Show a => Show (Automaton a) where
    show (A init finals transes) = 
        unlines $  [ "Initial: " ++ show init
                   , "Finals:  " ++ show finals 
                   , "Transitions: " ]
                   ++ map showTr transes
        where
          showTr (a,Eps b)   = show a ++ " --> " ++ show b 
          showTr (a,Lab c b) = show a ++ " --[" ++ show c ++ "]--> " ++ show b 

-- | Datatype for states used internally for the production construction.
data S a = SUnit a 
         | SProd (S a) (S a)
  deriving (Show,Eq,Ord)
    
-- | Compile regular experessions to finite state automata 
compile :: Reg -> Automaton Int
compile x = 
    numericState $ evalState (go $ opt x) 2
    where
      numericState a = evalState (renumber a) 0 

      newInt = do { i <- get; put (i+1); return i }

      opt (ReOr x y) =
          case (opt x, opt y) of 
            (ReCharSet cs, ReCharSet ds) -> ReCharSet $ CS.union cs ds 
            (ReNull, y') -> y' 
            (x', ReNull) -> x'
            (x',y')                      -> ReOr x' y'
      opt (ReAnd x y) = 
          case (opt x, opt y) of 
            (ReCharSet cs, ReCharSet ds) -> ReCharSet $ CS.intersect cs ds 
            (x', y')                     -> ReAnd x' y'
      
      opt (ReNot x) =
          case opt x of 
               ReNot x'     -> x'
               x'           -> ReNot x'

      opt (ReThen x y) =
          case (opt x, opt y) of
            (ReEps, y') -> y'
            (x', ReEps) -> x'
            (ReNull,y') -> ReNull
            (x',ReNull) -> ReNull 
            (x',    y') -> ReThen x' y'

      opt (ReRep x) = 
          case opt x of 
            ReEps    -> ReEps 
            ReNull   -> ReNull
            ReRep x' -> ReRep x'
            x'       -> ReRep x'

      opt ReNull = ReNull 
      opt ReEps  = ReEps 
      opt (ReCharSet cs) = ReCharSet cs 

      go :: Reg -> State Int (Automaton Int)
      go (ReCharSet cs) = 
          do { i <- newInt
             ; j <- newInt 
             ; return $ A i [j] [(i,Lab cs j)] }
      go (ReThen x y) =
          do { A init1 finals1 trans1 <- go x 
             ; A init2 finals2 trans2 <- go y 
             ; return $ A init1 finals2 
                          ([(f,Eps init2) | f <- finals1 ] ++ trans1 ++ trans2)}

      go (ReOr x y) =
          do { A init1 finals1 trans1 <- fmap eliminateEps $ go x 
             ; A init2 finals2 trans2 <- fmap eliminateEps $ go y 
             ; j <- newInt 
             ; return $ A j 
                          (finals1++finals2)
                          ((j,Eps init1):(j,Eps init2):trans1++trans2) }

      go (ReAnd x y) =
          do { A init1 finals1 trans1 <- fmap eliminateEps $ go x 
             ; A init2 finals2 trans2 <- fmap eliminateEps $ go y 
             ; let trans = sort $ productConstruction (init1,init2) (trans1,trans2)
             ; renumber $ eliminateUnreachable $ 
                          A (init1,init2) 
                            [ s | s@(i,j) <- states trans, 
                                 elem i finals1 && elem j finals2 ]
                            trans }
      go ReNull =
          return $ A 0 [] [] 
      go ReEps = 
          return $ A 1 [1] []
      go (ReRep x) =
          do { A init finals trans <- go x 
             ; i <- newInt 
             ; return $ A i [i]
                          ((i,Eps init):[ (f,Eps i) | f <- finals ]++trans)}
      go (ReNot x) = 
          do { a  <- go x 
             ; let a' =  complement $ determinize $ eliminateEps a
             ; renumber a' }

-- | A variant of |nub| for lists of |Ord| instances
snub :: Ord a => [a] -> [a]       
snub s = map head $ group $ sort s

states :: Ord a => [(a,Trans a)] -> [a]
states tr = snub $ concatMap (\(x,y) -> [x,f y]) tr 
    where
      f (Eps a)   = a 
      f (Lab _ a) = a

renumber :: Ord a => Automaton a -> State Int (Automaton Int)
renumber (A i f t) = 
    do { is <- mapM (\_ -> do { i <- get; put (i+1); return i }) sts
       ; let mapping = zip sts is 
       ; let m a = fromJust $ lookup a mapping 
       ; return $ A (m i) (map m f) (sort [ (m a, fmap m b) | (a,b) <- t ])}
    where
      sts = snub $ i:f++states t 

eliminateUnreachable (A init finals transes) = 
    A init (filter isReachable finals) (filter isReachableBoth transes)
    where
      isReachableBoth (i,t) = isReachable i && f t 
          where f (Eps j) = isReachable j 
                f (Lab _ j) = isReachable j 

      connected = concat [ [(i,[i,dst t]), (dst t, [dst t])] | (i,t) <- transes ]

      (connGraph, dec, enc) =
          graphFromEdges $ map (\(i,is) -> (i,i,is)) $
                           M.toList $ M.fromListWith (++) connected

      reachableStates = 
          case enc init of 
            Nothing -> []
            Just v  -> 
                [ k  | (_,k,_) <- map dec $ reachable connGraph v ]
      isReachable i = i `elem` reachableStates
          


-- eliminateEps :: Ord a => Automaton a -> Automaton a 
eliminateEps (A i f t) = eliminateUnreachable $
    A i (concatMap closureOf f) (concatMap c nonEps)
    where
      c (i,Lab c j) = [ (i',Lab c j) | i' <- closureOf i ]
      eps = concat [ [(j,[j,i]), (i,[i])]  | (i,Eps j) <- t ]
      (epsGraph, dec, enc) = 
          graphFromEdges $ map (\(i,is) -> (i,i,is)) 
                     $ M.toList $ M.fromListWith (++) eps 
              -- where
              --   f x = let (i,is) = foldr (\(i,j) (_,r) -> (i,j:r)) (undefined,[]) x
              --         in (i,i,i:is)
      closureOf i = 
          case enc i of 
            Nothing -> [i] 
            Just v  -> 
                [ k  | (_,k,_) <- map dec $ reachable epsGraph v ]
      nonEps = [ s | s@(_,Lab _ _) <- t ]
          
               
-- TODO: Efficient implementation 
productConstruction (i,j) (tr1,tr2) = 
    [ ((j1,j2), Lab common (j1',j2'))
          | (j1,Lab c1 j1') <- tr1
          , (j2,Lab c2 j2') <- tr2
          , common <- [CS.intersect c1 c2]
          , not (CS.isEmpty common) ] 

-- determinize :: Ord a => Automaton a -> Automaton (S.Set a)
determinize (A init finals trans) = -- (\s -> trace (show (A init finals trans) ++ show s) s)$
    let (sts, trs) = go (S.singleton init) [] [] 
        finalsS    = S.fromList finals 
        deadStates = sts L.\\ [ i | (i,_) <- trs ]
        deadTrans  = [ (i, Lab CS.universal S.empty) | i <- deadStates ] 
    in A (S.singleton init) 
         [ s | s <- sts, not $ S.null (S.intersection finalsS s) ]
         (deadTrans ++ trs)
    where
      combineTranses x y =
          do { (c1, j1) <- x
             ; (c2, j2) <- y
             ; let common = CS.intersect c1 c2 
             ; if CS.isEmpty common then 
                   []
               else 
                   return (common, j1 `S.union` j2) }
      go s generated trans
          | s `elem` generated = (generated, trans)
          | otherwise = 
              let relatedTranses =
                      map lookupTrans (S.toList s) 
                  newTrans = if L.null relatedTranses then 
                                 []
                             else 
                                 L.foldr1 combineTranses relatedTranses 
                  trs = [ (s,Lab c t) | (c,t) <- newTrans ]
                  nextStates = [ j | (_,j) <- newTrans ]
              in foldr (\j (gen, tr) -> go j gen tr) (s:generated, trs++trans) nextStates
      lookupTrans a =
          case M.lookup a transMap of
            Just v -> v 
            Nothing -> [(CS.universal, S.empty)]

      transMap = M.map completion $ M.fromListWith separateTranses $ 
                    (map (\(i,Lab c j) -> (i,[(c,S.singleton j)])) trans 
                     ++ deadTrans)
          where
            sts       = states trans 
            deadState = sts L.\\ [ i | (i,_) <- trans ]
            deadTrans = [ (i,[ (CS.universal, S.empty) ]) | i <- deadState ]
      
      separateTranses x y = snub $ 
          do { (c1, j1) <- x
             ; (c2, j2) <- y
             ; let common = CS.intersect c1 c2 
             ; let propC1 = CS.intersect c1 (CS.neg common)
             ; let propC2 = CS.intersect c2 (CS.neg common)
             ; let u c j = if CS.isEmpty c then [] else return (c,j)
             ; u common (j1 `S.union` j2) `mplus` u propC1 j1 `mplus` u propC2 j2  }

                       
      completion cjs = 
          let common = foldr (\(c,_) r -> CS.union c r) CS.empty cjs
              rest   = CS.neg common
          in -- trace (">> " ++ show cjs ++ " " ++ show rest) $
             if CS.isEmpty rest then 
                 cjs
             else 
                 (rest,S.empty):cjs 

            

complement (A init finals transes) = 
    -- Assumes that the input automaton is complete
    A init (sts L.\\ finals) transes
    where
      sts      = snub $ init:finals++states transes
      

-- ident = eitherOf (map rchar ['a'..'z'])
--         <> many (eitherOf (map rchar $ ['a'..'z'] ++ ['0'..'9']))

-- alphas = many (eitherOf $ map rchar $ ['a'..'z'] ++ ['A'..'Z'])

-- test1 = compile $ many $ rchar 'a'
-- test2 = compile $ rtext "then"
-- test3 = compile $ rtext "then" `union` rtext "else" `union` rtext "if"
-- test4 = compile $ many (eitherOf (map rchar ['a'..'z']))
--                   \\ rtext "if" \\ rtext "else" \\ rtext "then"


eliminateDead :: Ord a => Automaton a -> Automaton a 
eliminateDead (A init finals transes) =
    A init finals (filter isUseful transes)
    where
      invConn = concat [ [(dst t,[dst t,i]), (i,[i])]  | (i,t) <- transes ]
      (invConnGraph, dec, enc) = 
          graphFromEdges $ map (\(i,is) -> (i,i,is)) 
                     $ M.toList $ M.fromListWith (++) invConn

      usefuls = concatMap f finals
          where
            f i = case enc i of
                    Nothing -> [i]
                    Just  v -> [ k | (_,k,_) <- map dec (reachable invConnGraph v) ]

      isUseful (i,t) = (i `elem` usefuls) && (dst t `elem` usefuls)

      
          
    

toCFG :: Ord a => Automaton a -> State Int (Name, PS.ParseProg) 
toCFG am =
    do { A init finals transitions <- 
             renumber $ eliminateDead $ determinize $ eliminateEps am
       ; let exps  = map (conv finals) 
                      $ M.toList $ M.fromListWith (++) [ (i,[(c,j)]) | (i, Lab c j) <- transitions]
--       ; let fexps = map (\i -> PS.ParseDecl (toName i) $ PS.ParseFmap PS.SEmpty [PS.ParseEmpty]) finals 
       ; return (toName init, exps) }
    where
      toName i = Name $ "___reg" ++ show i 

      alts []     = PS.ParseEmpty 
      alts [x]    = x 
      alts (x:xs) = PS.ParseAlt x (alts xs)

      conv fs (i,cs) =
          PS.ParseDecl (toName i) $ alts $ 
            if i `elem` fs then 
                eps:map f cs 
            else 
                map f cs 
              where
                eps = PS.ParseFmap PS.SEmpty [PS.ParseText ""]
                f (c,j) = PS.ParseFmap PS.SAppend [ PS.ParseFmap PS.SSingle [PS.ParseCharOf c]
                                                  , PS.ParseNTerm (toName j)]

          