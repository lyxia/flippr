{
{-# LANGUAGE NoMonomorphismRestriction #-}
module Parser where 

import Lexer
import Syntax 
import Name 
}

%name pProg Prog 
%name pExp  Exp 

%tokentype { Token SourceRange  }
%error     { parseError }

%token 
    ";"      { Semi _ } 
    "="      { Sym _ "="  }

    "<>"     { Sym _ "<>" }
    -- "<>*"    { Sym _ "<>*" }
    -- "<>+"    { Sym _ "<>+" }

    "group"  { Ident _ "group" }
    "line"   { Ident _ "line" }
    "nest"   { Ident _ "nest" }
    "text"   { Ident _ "text" } 

    bq        { Sym _ "`" }

    "@@"     { Sym _ "@@" } 

    "charOf" { Ident _ "charOf" }

    -- "anyChar" { Ident _ "anyChar" }
    -- "none"    { Ident _ "none" } 

    -- "is"      { Ident _ "is" } 
    -- "andalso" { Ident _ "andalso" } 
    -- "orelse"  { Ident _ "oreelse" } 
    -- "butnot"  { Ident _ "butnot"  } 

    "//"     { Sym _ "//" }
    "<+"     { Sym _ "<+" }
    "("      { Sym _ "("  }
    ")"      { Sym _ ")"  }
    "["      { Sym _ "["  }
    "]"      { Sym _ "]"  }
    ":"      { Sym _ ":"  }
    ","      { Sym _ ","  }
    "$"      { Sym _ "$"  }

    "bij"    { Key _ "bij" }
    "if"     { Key _ "if" }
    "then"   { Key _ "then" }
    "else"   { Key _ "else" }

    "<="     { Sym _ "<=" }
    ">="     { Sym _ ">=" }
    "<"      { Sym _ "<"  }
    ">"      { Sym _ ">"  }
    "=="     { Sym _ "==" }
    "/="     { Sym _ "/=" }

    "&&"     { Sym _ "&&" }
    "||"     { Sym _ "||" }

    "|"      { Sym _ "|" } 

    "_"      { Wild _ }

    char     { Char _ _ }
    ident    { Ident  _ _ } 
    cident   { CIdent _ _ }
    string   { String _ _ }
    number   { Number _ _ }

%%

Prog : Decl Semi Prog  { $1 : $3 }
     | Decl            { $1 : [] }
     |                 {   []    }

Semi : ";" Semi {()}
     | ";"      {()}

Decl : ident ZPats "=" Exp { Decl (mTE $1 $4) (Name $ str $1) $2 $4 }

Pat : ident               { PVar (mTT $1 $1) (Name $ str $1) } 
    | cident              { PCon (mTT $1 $1) (Name $ str $1) [] }
    | "_"                 { PWild (info $1) }
    | "(" cident Pats ")" { PCon (mTT $1 $4) (Name $ str $2) $3 }
    | "(" LPat ")"        { $2 }
    | "(" CPats ")"       { case $2 of 
                              []  -> PCon (mTT $1 $3) (Name "()") []
                              [x] -> x 
                              xs -> PCon  (mTT $1 $3) (Name $ replicate (length xs) ',') xs }
    | "[" "]"             { PCon (m $1 $2) (Name "Nil") [] }


LPat : Pat ":" LPat1 { PCon (mergeRange (info $1) (info $3)) (Name "Cons") [$1,$3] }

LPat1 : Pat ":" LPat1 { PCon (mergeRange (info $1) (info $3)) (Name "Cons") [$1,$3] }
      | Pat           { $1 } 

Pats : Pat Pats  { $1 : $2 }
     | Pat       { [$1]  }

ZPats : Pats { $1 }
      |      { [] }

CPats : Pat "," CPats { $1 : $3 }
      | Pat           { $1 : [] }
      |               { [] }

Exp : ident ZAtoms "$" Exp            { EVar (mTE $1 $4) (Name (str $1)) ($2++[$4]) }
    | "nest" Atom "$" Exp             { ENest (mTE $1 $4) $2 $4 } 
    | "text" "$"   Exp                { EText (mTE $1 $3) $3 }
    | "text" "$"   App "@@" App   { EReify (m $1 $5) $5 $3 }
    | "group" "$"  Exp                { EGroup (mTE $1 $3) $3 }
    | "if" Exp "then" Exp "else" Exp  { EIf (mTE $1 $6) $2 $4 $6 }
    | "bij" ident "$" Exp             { EBij (mTE $1 $4) (Name (str $2)) $4 }
    | BinTop                          { $1 }

BinTop : BinTop bq ident bq Cond    { EVar (m $1 $5) (Name (str $3))  [$1,$5] }
       | Cond                           { $1 } 
 
Cond : App "==" App    { EVar (mEE $1 $3) (Name "==") [$1,$3] }
     | App "<=" App    { EVar (mEE $1 $3) (Name "<=") [$1,$3] }
     | App "/=" App    { EVar (mEE $1 $3) (Name "/=") [$1,$3] }
     | App ">=" App    { EVar (mEE $1 $3) (Name ">=") [$1,$3] }
     | App "<"  App    { EVar (mEE $1 $3) (Name "<")  [$1,$3] }
     | App ">"  App    { EVar (mEE $1 $3) (Name ">")  [$1,$3] }
     | ACond "||" Cond { EVar (mEE $1 $3) (Name "||") [$1,$3] }
     | ACond           { $1 }

ACond : App "&&" ACond { EVar (mEE $1 $3) (Name "&&") [$1,$3] }
      | Alt            { $1 }


Alt : Seq "<+" Alt { EAltL (mEE $1 $3) $1 $3 }
    | Seq          { $1 } 

Seq : App "<>" Seq  { ESeq (mEE $1 $3) $1 $3 }
    | App           { $1 } 

App : ident  Atoms       { EVar (mTE $1 (last $2)) (Name (str $1)) $2 } 
    | cident Atoms       { ECon (mTE $1 $ last $2) (Name (str $1)) $2 }
    | "nest" Atom Atom   { ENest (mTE $1 $3) $2 $3 }
    | "text" Atom        { EText (mTE $1 $2) $2 } 
    | "text" "(" App "@@" App ")" { EReify (m $1 $6) $5 $3}
    | "group" Atom       { EGroup (mTE $1 $2) $2 } 
    | "charOf" Atom      { ECharOf (m $1 $2) $2 }
    | "bij" ident Atom   { EBij (mTE $1 $3) (Name (str $2)) $3 }
    | Atom               { $1 }

Atom : ident       { EVar (mTT $1 $1) (Name $ str $1) [] }
     | cident      { ECon (mTT $1 $1) (Name $ str $1) [] }
     |  "line"     { ELine (mTT $1 $1) }
     | char        { EChar (info $1) (char $1) }
     | "(" CExps ")" { case $2 of { [] -> ECon (mTT $1 $3) (Name "()") []
                                  ; [x] -> x
                                  ; xs -> ECon (mTT $1 $3) (Name $ replicate (length xs) ',') xs } }
     | number      { EInt (mTT $1 $1) (num $1) } 
     | string      { EString (mTT $1 $1) (str $1) }

CExps: Exp "," CExps { $1 : $3 }
     | Exp           { $1 : [] }

Atoms : Atom Atoms { $1 : $2 }
      | Atom       { $1 : [] } 

ZAtoms : Atoms { $1 }
       |       { [] } 

{
num (Number _ n) = n 
str (String _ n) = n 
str (Ident _ n) = n 
str (CIdent _ n) = n 
char (Char _ n) = n

m x y = mergeRange (info x) (info y)
mTT = m
mTE = m
mET = m
mEE = m

parseError :: [Token SourceRange] -> a
parseError [] = error $ "Parse error at the end" 
parseError (tok@(t:_)) = error $ 
    let r = info t 
    in "Parse Error at : " ++ show r ++ "\n" ++ 
       "            near tokens: " ++ show (take 5 tok)
                                   ++ if length tok > 5 then " ... " else "" 

parseProg = pProg . alexScanTokens
parseExp  = pExp  . alexScanTokens
}
