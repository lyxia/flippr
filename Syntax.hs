{-|
This module defines datatype for ASTs of pretty-printers.
-}

{-# LANGUAGE DeriveDataTypeable, DeriveFunctor, DeriveFoldable, DeriveTraversable #-}

module Syntax where 


import qualified Data.Set as S 
import Data.Typeable 

import Data.Traversable 
import Data.Foldable hiding ( concatMap ) 

import Data.List ( (\\) )

import Control.Applicative 

import Data.Composable 
import Text.WadlerPpr 

import Name 

-- | Location in a source code 
data SourceRange
    = SourceRange 
      (Int,Int,Int) -- Start 
      (Int,Int,Int) -- End 
      deriving (Eq,Ord)

instance Show SourceRange where
  show (SourceRange (off1,l1,c1) (off2,l2,c2)) 
        | l1 == l2 =
            "Line: " ++ show l1 ++ " (" ++ show c1 ++ "--" ++ show c2 ++ ")"
        | l1 /= l2 =
            "Lines: " ++ show l1 ++ "(" ++ show c1 ++ ") -- " ++ show l2 ++ "(" ++ show c2 ++ ")"
                          

mergeRange (SourceRange s1 _) (SourceRange _ e2) = SourceRange s1 e2  
    

class ExtraInfo f where 
    info :: f a -> a 
 
type Prog a = [ Decl a ] 

data Decl a = Decl a Name [Pat a] (Exp a) -- The first exp represents "guard"
          deriving (Show, Functor, Foldable, Traversable)

instance ExtraInfo Decl where
    info (Decl i _ _ _) = i 

data Pat a = PVar  a Name 
           | PCon  a Name [Pat a] 
           | PWild a 
           deriving (Show, Functor, Foldable, Traversable) 

instance ExtraInfo Pat where 
    info (PVar i _)   = i
    info (PCon i _ _) = i 
    info (PWild i)    = i 

-- TODO :: Consider the case when an input contain a string, integer or other values ...-- TODO :: ... 

data Exp a
    = EVar   a Name [Exp a]       -- ^ A variable or a function call
    --- Syntax sugars? for contextual information 
    | ECon   a Name [Exp a]             -- ^ Constructor Application
    | EIf    a (Exp a) (Exp a) (Exp a)  -- ^ If expression
    --- Wadlers's Pritty-Printing Combinators 
    | ESeq   a (Exp a) (Exp a)    -- ^ Wadler's <>
    | ELine  a                    -- ^ Wadler's line
    | EGroup a (Exp a)            -- ^ Wadler's group 
    | ENest  a (Exp a) (Exp a)    -- ^ Wadler's nest 
                                  -- The first Exp must be a literal for simplicity
    | EText  a (Exp a)            -- ^ Wadler's text 
    --- Our Extension 
    | EAltL  a (Exp a) (Exp a)    -- ^ Left-biased choice
    | EReify a (Exp a) (Exp a)    -- ^ Assert that the 2nd expression is in the set represented by the 1st expression. 
    | ECharOf a (Exp a)           
    | EBij a Name (Exp a)         -- ^ Bijection. Name is not considered as a free variable.
    | EInt   a Int           -- ^ For ENest 
    | EString a String       -- ^ For EText 
    | EChar a Char           -- ^ For is or obtained by matching 
---    --- Char set expression 
---   | EIs a (Exp a)       -- Char testing expression 
      deriving (Show, Functor, Foldable, Traversable, Eq, Ord) 


instance Composable (Exp a) where
    compos f (EVar a n es)    = EVar a n <$> traverse f es 
    compos f (ECon a n es)    = ECon a n <$> traverse f es 
    compos f (EIf a e1 e2 e3) = EIf a <$> f e1 <*> f e2 <*> f e3 
    compos f (ESeq a e1 e2)   = ESeq a <$> f e1 <*> f e2
    compos f (ELine a)        = pure $ ELine a 
    compos f (EGroup a e1)    = EGroup a <$> f e1 
    compos f (ENest a e1 e2)  = ENest a <$> f e1 <*> f e2 
    compos f (EText a e)      = EText a <$> f e 
    compos f (EAltL a e1 e2)  = EAltL a <$> f e1 <*> f e2 
    compos f (EReify a e1 e2) = EReify a <$> f e1 <*> f e2 
    compos f (ECharOf a e)    = ECharOf a <$> f e
    compos f (EBij a n e)     = EBij a n <$> f e
    compos f (EInt a i)       = pure $ EInt a i 
    compos f (EString a s)    = pure $ EString a s 
    compos f (EChar a s)      = pure $ EChar a s 
---    compos f (EIs a e)        = EIs a <$> f e 
    --- compos f (ERE a s)        = pure $ ERE a s

-- Retreive "additional information"
instance ExtraInfo Exp where 
    info (EVar a _ _)   = a
    info (ECon a _ _)   = a 
    info (EIf  a _ _ _) = a 
    info (ESeq a _ _)   = a
    info (ELine a)      = a
    info (EGroup a _)   = a 
    info (ENest a _ _)  = a
    info (EText a _)    = a
    info (EAltL a _ _)  = a
    info (EReify a _ _) = a
    info (ECharOf a _)  = a
    info (EBij a _ _)   = a 
    info (EInt a _)     = a
    info (EString a _)  = a
    info (EChar a _)    = a 
--    info (EIs a _)      = a 

-- | |funcs p| returns the names of the functions defined in |p|.
funcs :: Prog a -> [Name]
funcs decls = S.toList $ S.fromList $ concatMap funcsD decls
    where 
      funcsD d@(Decl _ f ps e) = f:freeVarsRHS d

-- | Free variables in a pattern 
freeVarsP :: Pat a -> [Name]
freeVarsP (PVar _  x)   = [x]
freeVarsP (PCon _ _ ps) = concatMap freeVarsP ps 
freeVarsP _             = []

-- | Free variables in a right-hand side. 
freeVarsRHS :: Decl a -> [Name]
freeVarsRHS (Decl _ f ps e) = S.toList $ S.fromList $
    freeVars e \\ concatMap freeVarsP ps

-- | Free variables in an expression.
freeVars :: Exp a -> [Name]
freeVars exp = S.toList $ S.fromList $ f exp
    where
      f :: Exp a -> [Name]
      f (EVar _ n es) = n:concatMap f es 
      f e = composFold f e

forgetLayout :: Prog a -> Prog a 
forgetLayout prog = map forgetD prog 
    where
      forgetD (Decl a f ps e) = Decl a f ps (forgetE e)

      forgetE :: Exp a -> Exp a 
      forgetE (ELine a)      = EVar a (Name "__line__") []
      forgetE (EGroup _ e)   = forgetE e 
      forgetE (ENest _ _ e)  = forgetE e 
      forgetE e              = composOp forgetE e 


---
pprProg ds = foldDoc (\x y -> x <> text "\n\n" <> y) (map pprD ds) 
pprD (Decl _ f ps e) = 
    group (text (show f) <+> foldDoc (<+>) (map pprPat ps) <+> text "=") <>
          nest 4 (line <> pprExp 0 e)

precAlt = 5
precSeq = 6 
precAnn = 8 
precApp = 9      

pprPat (PVar _ x) = text (show x)
pprPat (PWild _)  = text "_"
pprPat (PCon _ (Name "Cons") [x,y]) = parens (pprPat x <> text ":" <> pprPat y)
pprPat (PCon _ (Name "Nil")  [])    = text "[]"
pprPat (PCon _ c ps)         = parens (text (show c) <+> 
                                                foldDoc (<+>) (map pprPat ps))

parensWhen b d = if b then parens d else d 
                 
asBin j i op [x1,x2] b = 
    let l = if b < 0 then -1 else 0 
        r = if b > 0 then -1 else 0 
    in parensWhen (i >= j) $ pprExp (j+1) x1 <+> text op <+> pprExp (j+1) x2 

pprExp i (EVar _ f [])   = text (show f) 

pprExp i (EVar _ (Name f) xs)   = 
          case f of 
            "==" -> asBin precAlt i "==" xs 0
            "/=" -> asBin precAlt i "==" xs 0
            "<=" -> asBin precAlt i "<=" xs 0 
            ">=" -> asBin precAlt i ">=" xs 0
            "<"  -> asBin precAlt i "<" xs 0
            ">"  -> asBin precAlt i ">" xs 0 
            "andalso" -> h "andalso" xs
            "orelse"  -> h "orelse" xs
            "butnot"  -> h "butnot" xs 
            "union"   -> h "union" xs
            "intersect" -> h "intersect" xs 
            "\\\\" -> asBin precAlt i "\\\\" xs (-1) 
            _ ->
                group $ parensWhen (i >= precApp) $ text f <+> foldDoc (<+>) (map (pprExp precApp) xs)
    where
      h s [e1,e2] = 
          parensWhen (i >= precAlt) $ 
                               pprExp (precAlt-1) e1 <+> text ("`"++s++"`") 
                               <+> pprExp precAlt e2
-- pprExp i (EVar _ f xs)   = group $ parensWhen (i >= precApp) $ text (show f) <+> foldDoc (<+>) (map (pprExp precApp) xs)
pprExp i (ECon _ c [])   = text (show c) 
pprExp i (ECon _ c xs)   = group $ parensWhen (i >= precApp) $ text (show c) <+> foldDoc (<+>) (map (pprExp precApp) xs)
pprExp i (EIf _ e1 e2 e3)= group $ text "if" <+> pprExp 0 e1 <+> text "then" 
                                        <> nest 4 (line <> pprExp 1 e2)
                                        <> line <> text "else" 
                                        <> nest 4 (line <> pprExp 0 e3)
-- pprExp i (EPrim _ f [])   = text (show f) 

-- pprChExp i (EPrim _ f es) = 
--     case f of 
--       Name "andalso" | length es == 2 -> 
--           h "andalso" (es !! 0) (es !! 1)
--       Name "orelse"  | length es == 2 ->
--           h "orelse"  (es !! 0) (es !! 1) 
--       Name "butnot"  | length es == 2 ->
--           h "butnot"  (es !! 0) (es !! 1)
--       _ | length es == 0 ->
--            text (show f)


pprExp i (ESeq _ e1 e2)  = group $ parensWhen (i >= precSeq) $ 
                                  pprExp (precSeq-1) e1 </> text "<>" <+> nest 3 (pprExp (precSeq-1) e2)
pprExp i (ELine _)       = text "line"
pprExp i (EGroup _ e)    = parensWhen (i >= precApp) $ group $ 
                                text "group" </> nest 2 (pprExp precApp e)
pprExp i (ENest _ e1 e2) = parensWhen (i >= precApp) $ 
                                 group (text "nest" <+> pprExp precApp e1 </> nest 2 (pprExp precApp e2))

pprExp i (EText _ e)     = parensWhen (i >= precApp) $ 
                                 text "text" <+> pprExp precApp e
pprExp i (EAltL _ e1 e2) = parensWhen (i >= precAlt) $ group $ 
                                 pprExp (precAlt-1) e1 </> text "<+" </> pprExp (precAlt-1) e2 
                                        
pprExp i (EReify _ e1 e2) = parensWhen (i >= precAlt) $ group $ 
                                 pprExp precAnn e2 <+> text "@@" <+> pprExp precApp e1

pprExp i (EBij _ n e)     = parensWhen (i >= precApp) $ 
                              text "bij" <+> text (show n) <+> pprExp precApp e

pprExp i (ECharOf _ n)    = parensWhen (i >= precApp) $ 
                                 text "charOf" <+> pprExp precApp n 
pprExp i (EChar _ c) = text (show c)

pprExp i (EInt _ j)       = text (show j)
pprExp i (EString _ s)    = text (show s)

-- pprExp i (EIs _ c) = parensWhen (i >= precApp) $ 
--                            text "is" <+> pprExp precApp c

