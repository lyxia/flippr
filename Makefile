SRC = $(shell find . -name \*.hs)
TARGET = FliPpr 
DISTDIR= FliPpr-0.0.2

all : $(TARGET)

$(TARGET): Parser.hs Lexer.hs $(SRC)
	ghc -o $(TARGET) --make Main +RTS -H 

Parser.hs : Parser.y
	happy -gca Parser.y -ipout.txt

Lexer.hs : Lexer.x 
	alex Lexer.x 

Memo.html : Memo
	pandoc -c memo.css -s Memo > Memo.html 

README.html : README
	pandoc -c memo.css -s README > README.html

tar : clean
	mkdir $(DISTDIR)
	mkdir $(DISTDIR)/Examples
	mkdir $(DISTDIR)/CodeGen
	cp -r *.hs $(DISTDIR)
	cp -r Examples/* $(DISTDIR)/Examples/
	cp -r CodeGen/*  $(DISTDIR)/CodeGen/
	cp -r *.x  $(DISTDIR)
	cp -r *.y  $(DISTDIR)
	cp -r Makefile $(DISTDIR)
	cp -r README   $(DISTDIR)
	tar czvf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

clean:
	rm -f *.hi
	rm -f *.o 
	rm -f */*.hi
	rm -f */*.o
	rm -f */*/*.hi
	rm -f */*/*.o
	rm -f *.prof
	rm -f $(TARGET)
	rm -f Lexer.hs
	rm -f Parser.hs 
	rm -f Memo.html
	rm -f pout.txt
