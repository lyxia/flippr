{-|
This module implements program inversion used in FliPpr. 
-}
module Inverter (invert,evalCE) where
      
import Data.List hiding (intersect, union, (\\))
import Syntax
import Name 
import ParserSyntax 

import Control.Monad.State 
import RegularGrammar 
import RegexParser 

import Typing 

import qualified Data.Map as M 

import Data.CharSet (CharSet)
import qualified Data.CharSet as CS 

invert :: Ord a => Prog a -> [(Name,[Type],Type)] -> (Name, ParseProg)
invert prog_ sign = 
    let ( decls, (decls',_,_) ) = runState (mapM inv functions) ([],0,M.empty)
    in (main, decls ++ decls')
    where 
      prog = forgetLayout prog_
      main = head [ f | Decl _ f _ _ <- prog ]
      functions = nub [ f | Decl _ f _ _ <- prog ]

      tMap = [ (f,(ts,t)) | (f,ts,t) <- sign ]
      signOf f = case lookup f tMap of 
                   Just t -> t 
                   Nothing -> error $ "The type of " ++ show f ++ " is unknown." 

      alts []     = ParseEmpty 
      alts [x]    = x 
      alts (x:xs) = ParseAlt x (alts xs)

      p2p (PVar _ x)    = PPVar x 
      p2p (PCon _ c ps) = PPCon c (map p2p ps)
      p2p (PWild _)     = PPPlaceHolder 

      inv f = 
          do { es <- mapM (\(g,ps,e,ty) ->
                               case ty of 
                                 ([],TyReg) -> 
                                     do { e' <- invRE e
                                        ; return $ e' }
                                 _ -> 
                                     do { e' <- invE e
                                        ; return $ ParseFmap (FReconst $ map p2p ps) [e']})
                       [ (g,ps,e,signOf g) | Decl _ g ps e <- prog, f == g ]
             ; return $ ParseDecl f $ alts es}

      invE (EVar _ f arg) = 
          let ns = [ n | EVar _ n [] <- arg ] 
          in return $ ParseFmap (FAssignF ns) [ParseNTerm f]

      invE (ESeq _ e1 e2) = 
          do { e1' <- invE e1
             ; e2' <- invE e2 
             ; return $ ParseFmap FAppend [e1',e2']}

      invE (EText _ (EString _ s))   
          = return $ ParseFmap FIgnore [ParseText s]

      invE (EReify _ e1 e2) = f e2 
          where f (EVar _ n []) = do { e1' <- invRE e1; return $ ParseFmap (FAssignS n) [e1']}
                f (EBij _ n  e) = do { e' <- f e; return $ ParseFmap (FBij n) [e'] }

      invE (EAltL _ e1 e2)       = liftM2 ParseAlt (invE e1) (invE e2)

      invE (ECharOf _ n) = return $ ParseFmap FIgnore [ParseCharOf (evalCE n)]

      invE e = error $ show $ pprExp 0 e 

      -- FIXME: Change it to take CharSet

      -- invChE (EVar _ (Name "is") [EChar _ c]) =
      --     PChIs c
      -- invChE (EVar _ f es) =
      --     PChPrim f (map invChE es)

            
      invRE (EVar _ f []) = do { return $ ParseNTerm f }
      invRE x =  do { (_,i,m) <- get 
                    ; let u = info x 
                    ; case M.lookup u m of 
                        Just n -> return $ ParseNTerm n
                        Nothing -> 
                            do { let ( (n,decls), i') = runState (toCFG (compile $ go x)) i
                               ; modify (\(x,_,m) -> (decls++x,100+i',M.insert u n m)) 
                               ; return $ ParseNTerm n }
                    }
          where
            go (EVar _ (Name "re") [EString _ s])  = parseRE s 
            go (EVar _ (Name "intersect") [e1,e2]) = intersect (go e1) (go e2)
            go (EVar _ (Name "union")     [e1,e2]) = union     (go e1) (go e2)
            go (EVar _ (Name "\\\\")      [e1,e2]) = go e1 \\ go e2 
            go (EVar _ (Name "many")      [e])     = many (go e) 
            go (EVar _ f []) =
                let e = head $ [ e | Decl _ g ps e <- prog, f == g ]
                in  go e -- FIXME: not good 
          

      -- invChE (CEIs _ c)   = PChIs c 
      -- invChE (CEAnd _ e1 e2) = PChAnd (invChE e1) (invChE e2)
      -- invChE (CEOr  _ e1 e2) = PChOr  (invChE e1) (invChE e2)
      -- invChE (CEWithout _ e1 e2) = PChWithout (invChE e1) (invChE e2)


evalCE (EVar _ (Name "chars") [EString _ s])        = parseClass s  
evalCE (EVar _ (Name "char")  [EChar _ c])          = CS.singleton c 
evalCE (EVar _ (Name "range") [EChar _ s, EChar _ e]) = CS.charSetRange s e 

evalCE (EVar _ (Name s) [e1,e2]) =
  case s of 
    "andalso" -> 
      CS.intersect (evalCE e1) (evalCE e2)
    "butnot" ->
      CS.intersect (evalCE e1) (CS.neg $ evalCE e2)
    "orelse" ->
      CS.union (evalCE e1) (evalCE e2)
    _ ->
      error $ "uninlined function: " ++ s ++ "?"
evalCE (EVar _ (Name s) []) =
  case s of 
    "allChars"         -> CS.universal
    "noChars"          -> CS.empty 
    "spaceChars"       -> CS.spaceChars
    "alphaChars"       -> CS.alphaChars
    "alphaNumChars"    -> CS.alphaNumChars 
    "lowerChars"       -> CS.lowerChars
    "upperChars"       -> CS.upperChars
    "printChars"       -> CS.printChars
    "digitChars"       -> CS.digitChars 
    "octDigitChars"    -> CS.octDigitChars
    "hexDigitChars"    -> CS.hexDigitChars
    "letterChars"      -> CS.letterChars
    "markChars"        -> CS.markChars
    "numberChars"      -> CS.numberChars 
    "punctuationChars" -> CS.punctuationChars 
    "symbolChars"      -> CS.symbolChars 
    "separatorChars"   -> CS.separatorChars
    "asciiChars"       -> CS.asciiChars
    "asciiLowerChars"  -> CS.asciiLowerChars
    "asciiUpperChars"  -> CS.asciiUpperChars
    "latin1Chars"      -> CS.latin1Chars
    _                  -> CS.empty 
