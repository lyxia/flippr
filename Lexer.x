{
module Lexer where 

import Syntax
}

%wrapper "posn"

$digit = 0-9
$alpha = [a-zA-Z]
@ident  = [_a-z] ($alpha | $digit | "_")* [']*
@cident = [A-Z] ($alpha | $digit | "_")* [']*

-- $graphic    = $printable # $white
-- @string     = \" ($graphic # \")* \"

@string = \" ( ($printable # [\"\\]) | "\" "\" | "\n" | "\t" | "\r" | "\" [\"] )* \" --"
@char = \' ( ($printable # [\'\\]) | "\" "\" | "\n" | "\t" | "\r" | "\" [\"] ) \' --"
@test = " "

tokens :- 
       $white+ ;
       "--".* ;
       ";"                   { wrap $ \p s -> Semi p } 
       "@@"                  { wrap $ Sym } 
       "("                   { wrap $ Sym } 
       ")"                   { wrap $ Sym }
       ","                   { wrap $ Sym }
       ":"                   { wrap $ Sym }
       "["                   { wrap $ Sym }
       "]"                   { wrap $ Sym }
       "bij"                 { wrap $ Key } 
       "if"                  { wrap $ Key }
       "then"                { wrap $ Key }       
       "else"                { wrap $ Key }           
       "`"                   { wrap $ Sym }
       [\=\+\-\*\/\<\>\$\|\&]+ { wrap $ Sym }
       @string               { wrap $ \p s -> String p (read s) } 
       @char                 { wrap $ \p s -> Char   p (read s) }
       $digit+               { wrap $ \p s -> Number p (read s) } 
       "_"                   { wrap $ \p s -> Wild p }   
       @ident                { wrap $ Ident  } 
       @cident               { wrap $ CIdent }

{
data Token a = Sym a String 
             | Key a String 
             | Ident a String 
             | CIdent a String 
             | String a String 
             | Number a Int 
             | Char a Char
             | Wild a 
             | Semi a 

instance Show (Token a) where 
    show (Sym _ s) = s 
    show (Key _ s) = s 
    show (Ident _ s) = s
    show (CIdent _ s) = s 
    show (String _ s) = show s 
    show (Number _ s) = show s 
    show (Char _ c)   = show c 
    show (Wild _) = "_" 
    show (Semi _) = ";"


    showList ss rest = foldr (\a r -> show a ++ " " ++ r) rest ss 

instance ExtraInfo Token where 
    info (Sym a _)    = a
    info (Key a _)    = a 
    info (Ident a _)  = a 
    info (CIdent a _) = a 
    info (String a _) = a 
    info (Number a _) = a 
    info (Semi a)     = a 
    info (Wild a)     = a 
    info (Char a _)   = a 

wrap f (AlexPn off l c) s
      = f (SourceRange (off,l,c) (off + length s, l, c + length s)) s
}
