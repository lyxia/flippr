{-# LANGUAGE NoMonomorphismRestriction #-}

{-|
The module implements the conversion from the surface language
to the core language in FliPpr. 


-}
module Desugar (desugar) where 

import Data.Graph 
import Data.Unique 

import Control.Monad.State 
import Data.Maybe (fromJust) 

import qualified Data.Traversable as T
import Prelude 

import qualified Data.Map as M 

import Debug.Trace 

import Data.List (nub, intersect, sortBy, (\\)) 
import Data.Function 

import Text.WadlerPpr 

import Typing 
import Name 
import Syntax 

import Data.Composable 

desugar = inline . superCompile . first assignUnique  
    where
      first f (x,y) = (f x,y)

type IDed a = Maybe (Int,a)

type Env a   = [(Name, TV a)]
data TV a = Thunk (Exp a) [Name] (Env a) -- For Document
          | Value (Exp a)                -- For Contextual Information 
            deriving (Eq,Ord) 

assignUnique :: Prog a -> Prog (IDed a) 
assignUnique prog = flip evalState 0 $ mapM goD prog 
    where
      newID = do { i <- get ; put (i+1) ; return i }
      goD = T.mapM (\a -> do { i <- newID; return $ Just (i,a) } )  
      

      
type EncodedEnv = [(Name, Enc)]
data Enc = EncThunk Int EncodedEnv 
         | EncValue (Exp ()) deriving (Eq,Ord)


isPrimitive :: Name -> Bool 
isPrimitive f = f `elem` prims
    where
      prims = [ f | (f,_,_) <- Typing.primitiveMapping ]

{-|
   An implementation of supercompilation specialized to our program.
-}
superCompile :: (Prog (IDed a), [(Name,[Type],Type)]) -> (Prog (IDed a), [(Name,[Type],Type)])
superCompile (prog, sign) = 
    let (_,_,signProg) = 
            execState (mapM (driveD [] [] undefined) mainDecl) (0,M.empty,[])
    in unzip $ rearrange $ reverse $ signProg 
    where
      mainFunc = head [ f | Decl _ f _ _ <- prog ]
      mainDecl = [ d | d@(Decl _ f _ _) <- prog, f == mainFunc ]

      rearrange :: [(Decl a, (Name,[Type],Type))] -> [(Decl a, (Name,[Type],Type))]
      rearrange sprog =
          [ (d,s) | ( (d@(Decl _ f _ _)) ,s) <- sprog, f == mainFunc ]
          ++ sortBy (compare `on` h) [ (d,s) | ( (d@(Decl _ f _ _)) ,s) <- sprog, f /= mainFunc ]
             where h ((Decl _ f _ _),_) = f

      tMap = [ (f,(ts,t)) | (f,ts,t) <- sign ]
      signOf f = case lookup f tMap of 
                   Just t -> t 
                   Nothing -> error $ "The type of " ++ show f ++ " is unknown." 

      idOf e = idOfI (info e)
      idOfI (Just (i,_)) = i 

      encodeEnv :: Env (IDed a) -> EncodedEnv 
      encodeEnv env = [ (x,conv y) | (x,y) <- shallowSort env ] 
          where
            conv (Thunk ex bv env) = EncThunk (idOf ex) (encodeEnv env)
            conv (Value v)         = EncValue (fmap (\_ -> ()) v) 
            shallowSort xs = sortBy (compare `on` fst) xs 
      
      uniqueInt :: State (Int,a,b) Int 
      uniqueInt = do { (i,m,r) <- get; put (i+1,m,r); return i }

      -- encodeIDEnv id []  = (-1,encodeEnv []) 
      -- encodeIDEnv id env = (idOfI id, encodeEnv env) 

      encodeFSargs f sargs = 
          (f, encodeEnv $ zip [ Name $ "_" ++ show i | i <- [1..] ] sargs )

      registerFunc f sargs g = 
          do { (i,m,r) <- get; put (i,M.insert (encodeFSargs f sargs) g m,r) }

      checkRegistered f [] | f == mainFunc = return $ Just f
      checkRegistered f sargs =
          do { (i,m,r) <- get; return $ M.lookup (encodeFSargs f sargs) m }

      addRule :: c -> State (a,b,[c]) ()
      addRule r = do {(i,p,rs) <- get; put (i,p,r:rs) }


      num (EInt _ i) = i 
      num e          = error $ "Not an integer: " ++ showExp e 

      eraseID = fmap (\_ -> ()) 

      showSArgs sargs = pretty 80 $ brackets $ foldDoc (\x y -> x <> text "," <> y) $ map pprTV sargs

      showEnv env = pretty 80 $ pprEnv env 

      pprEnv env = 
                brackets $
                   foldDoc (\x y -> x <> text "," <> y)
                        $ map (\(x,y) -> text (show x) <> text ":" <> pprTV y ) env 

      pprTV (Thunk e bv env) =  text "<" <> pprExp 0 e <> text "," <+> text (show bv) <> text "," <+> pprEnv env <> text ">"
      pprTV (Value e)        = pprExp 0 e 
      showExp e   = pretty 80 $ pprExp 0 e 

      unName (Name n) = n 

      newName f id sargs =
          case signOf f of 
            (ts,t) | all (\x -> x == TyInput)   ts -> return f -- no room to specialize 
            (ts,t) | null [ 0 | Thunk _ _ _ <- sargs ] 
                 && and [ isSimple v | Value v <- sargs ]  -> 
                     do n <- mapM conv [ v | Value v <- sargs ]
                        return $ Name $ "_" ++ (unName f) ++ "_" ++ concat n
                         where isSimple (ECon _ _ []) = True 
                               isSimple (EInt _ _)    = True 
                               isSimple _             = False 
                               conv (ECon _ c []) = return $ "_C" ++ show c 
                               conv (EInt _ i)    = return $ "_C" ++ show i 
            (ts,t) -> do {i <- uniqueInt; return $ Name $ "_" ++ (unName f) ++ "__" ++ show i}

      newVarName = do { i <- uniqueInt; return $ Name ("_x" ++ show i) }

      alphaRenameI (Decl id f ps e) =
          do { let (ts,t) = signOf f 
             ; (ps',envs) <- fmap unzip $ mapM g $ zip ps ts 
             ; return $ Decl id f ps' (replaceE (concat envs) e) }
          where 
            g (p,TyInput) = alphaRenameIP p 
            g (p,_)       = return (p,[])
            alphaRenameIP (PVar a x) = 
                do { x' <- newVarName 
                   ; return $ (PVar a x', [(x,EVar Nothing x' [])]) }
            alphaRenameIP (PCon a c ps) =
                do { (ps',envs') <- fmap unzip $ mapM alphaRenameIP ps 
                   ; return $ (PCon a c ps', concat envs') }
            alphaRenameIP (PWild a) =
                return (PWild a, []) 

--      driveD :: [TV (IDed a)] -> [Name] -> Name -> Decl (IDed a) 
--                -> State (Int, M.Map (Int,EncodedEnv (IDed a)) Name, [(Decl (IDed a), (Name,[Type],Type))]) ()

      driveD [] _ _ (Decl a f ps e) =
          do { let (ts,t) = signOf f 
             ; let bv     = nub $ concatMap freeVarsP ps
             ; e' <- driveE bv [] e 
             ; addRule ((Decl a f ps e'), (f,ts,t)) }
      driveD sargs fvs g (Decl a f ps e) =
          let (ts,t) = signOf f 
              sps    = [ p | (p,t) <- zip ps ts, t /= TyInput ]
              dps    = [ p | (p,t) <- zip ps ts, t == TyInput ]
              env_   = do { env' <- fmap concat $  zipWithM patternMatch sps sargs 
                          ; return $ sortBy (compare `on` fst) env' }
                       -- sortBy (compare `on` fst) $ zip [ x | PVar _ x <- sps ] sargs
              bv     = nub $ fvs ++ concatMap freeVarsP dps
          in case env_ of 
               Just env -> 
                   do { e' <- driveE bv env e 
                      ; let newInputPattern = [PVar Nothing n | n <- fvs]++dps
                      ; let newArgumentsType =
                                [TyInput | _ <- fvs] ++ [t | t <- ts, t == TyInput ]
                      ; addRule ( Decl a g newInputPattern e'
                                , (g, newArgumentsType ,t)) }
               Nothing ->
                   return () 

      patternMatch (PVar _ x) v = return [(x,v)]
      patternMatch p (Value v) = go p v 
          where
            go (PVar _ x)    v = return [(x,Value v)]
            go (PCon _ c ps) (ECon _ c' es) 
                | c==c' && length ps == length es  = fmap concat $ zipWithM go ps es
            go (PCon _ (Name "Nil") []) (EString _ [])
                = return []
            go (PCon _ (Name "Cons") [p1,p2]) (EString _ (c:cs)) 
                = do { b1 <- go p1 (EChar undefined c) 
                     ; b2 <- go p2 (EString undefined cs)
                     ; return $ b1 ++ b2 }
            go _ _ = fail "^_^;"



      restrictBv  e bv   = bv `intersect` freeVars e
      restrictEnv e env  = [ (x,v) | (x,v) <- env, x `elem` freeVars e ]

      deepFV e env = let v = freeVars e 
                     in v ++ concat [ freeVarsT e' | x <- v, Just e' <- [lookup x env] ]
          where freeVarsT (Thunk e bv env) = deepFV e env 
                freeVarsT (Value e)        = [] 

      driveE bv env (EVar id f es) 
          | Just v <- lookup f env -- f is a variable for document or context 
              = case v of 
                  Thunk e' bv' env' ->
                      driveE bv' env' e'
                  Value e' ->
                      return e' 
          | f `elem` bv -- f is an input variable (not reachable)
              = return $ EVar id f es -- leave it 
          | isPrimitive f 
              = let conv True  = ECon Nothing (Name "True") []
                    conv False = ECon Nothing (Name "False") []                
                in case (unName f) of 
                  "==" -> 
                      do { [e1,e2] <- mapM (driveE bv env) es 
                         ; return $ conv (eraseID e1 == eraseID e2) }
                  "/=" -> 
                      do { [e1,e2] <- mapM (driveE bv env) es 
                         ; return $ conv (eraseID e1 /= eraseID e2) }
                  "<" ->
                      do { [e1,e2] <- mapM (driveE bv env) es
                         ; return $ conv (num e1 < num e2) }
                  ">" ->
                      do { [e1,e2] <- mapM (driveE bv env) es 
                         ; return $ conv (num e1 > num e2) }
                  "<=" ->
                      do { [e1,e2] <- mapM (driveE bv env) es
                         ; return $ conv (num e1 <= num e2) }
                  ">=" -> 
                      do { [e1,e2] <- mapM (driveE bv env) es 
                         ; return $ conv (num e1 >= num e2) }
                  "not" ->
                      do { [e] <- mapM (driveE bv env) es 
                         ; return $ conv (eraseID e == eraseID (conv False)) }
                  _ ->  liftM  (EVar id f) $ mapM (driveE bv env) es 
          | otherwise  -- f is a function call 
              = do { let (ts,t) = signOf f 
                   ; let resolveD (EVar _ y []) =
                             return $ fromJust $ lookup y env 
                         resolveD e =
                             return $ Thunk e (deepFV e env `intersect` bv) (restrictEnv e env)
                         resolveC e = 
                             do v <- driveE (restrictBv e bv) (restrictEnv e env) e
                                return $ Value v 
                         -- resolveC (EVar _ y []) =
                         --     fromJust $ lookup y env 
                         -- resolveC e = 
                         --     Value e                              
                         freeVarsT (Thunk e bv env) = deepFV e env 
                         freeVarsT (Value e)        = [] 
                   ; sargs <- sequence [ case t of { TyDocument -> resolveD e; TyContext -> resolveC e }
                                                      | (e,t) <- zip es ts, t /= TyInput ]
                   ; let dargs = [ e | (e,t) <- zip es ts, t == TyInput ]
                   ; let freeInputVars = bv `intersect` (nub $ concatMap freeVarsT sargs) -- nub is not necessary because of linearity 
                   ; y <- checkRegistered f sargs -- id envRestricted
                   ; let gcall g = EVar Nothing g ([EVar Nothing n [] | n <- freeInputVars ]++dargs)
--                   ; trace ("SArgs> " ++ showSArgs sargs) (return ())
--                   ; trace (showEnv env ++ "," ++ show bv ++ " :- " ++ showExp (EVar id f es) ++ "\n  ->" ++ showExp (gcall (Name "XXX"))) (return ()) 
                   ; case y of 
                       Just g -> return $ gcall g 
                       Nothing -> 
                           do { g <- newName f id sargs 
                              ; registerFunc f sargs g -- id envRestricted g 
                              ; mapM_ (driveD sargs freeInputVars g <=< alphaRenameI) [ d | d@(Decl _ f' _ _) <- prog, f'==f ] 
                              ; return $ gcall g 
                              } 
                   } 
              
      -- driveE bv env (EPrim a s es) =
      --     case (unName s) of 
      --       "==" -> 
      --           do { [e1,e2] <- mapM (driveE bv env) es 
      --              ; return $ conv (eraseID e1 == eraseID e2) }
      --       "/=" -> 
      --           do { [e1,e2] <- mapM (driveE bv env) es 
      --              ; return $ conv (eraseID e1 /= eraseID e2) }
      --       "<" ->
      --           do { [e1,e2] <- mapM (driveE bv env) es
      --              ; return $ conv (num e1 < num e2) }
      --       ">" ->
      --           do { [e1,e2] <- mapM (driveE bv env) es 
      --              ; return $ conv (num e1 > num e2) }
      --       "<=" ->
      --           do { [e1,e2] <- mapM (driveE bv env) es
      --              ; return $ conv (num e1 <= num e2) }
      --       ">=" -> 
      --           do { [e1,e2] <- mapM (driveE bv env) es 
      --              ; return $ conv (num e1 >= num e2) }
      --       "not" ->
      --           do { [e] <- mapM (driveE bv env) es 
      --              ; return $ conv (eraseID e == eraseID (conv False)) }
      --       _ ->  liftM  (EPrim a s) $ mapM (driveE bv env) es 
      --     where
      --       conv True  = ECon Nothing (Name "True") []
      --       conv False = ECon Nothing (Name "False") []

      driveE bv env (EIf a e1 e2 e3) = 
          do { e1' <- driveE bv env e1 
             ; case e1' of 
                 ECon _ (Name "True") [] -> 
                     driveE bv env e2 
                 ECon _ (Name "False") [] ->
                     driveE bv env e3 
                 _ ->
                     error $ "Not speified well " ++ showEnv env ++ "\n" ++ showExp e1 }
      driveE bv env e = composM (driveE bv env) e 


replaceE :: [(Name,Exp a)] -> Exp a -> Exp a 
replaceE env e = go e 
    where
      go (EVar a f []) = 
          case lookup f env of Just v  -> v 
                               Nothing -> EVar a f []
      go e = composOp go e 
      -- go (EVar a f xs)    = EVar a f (map go xs)
      -- go (ECon a f xs)    = ECon a f (map go xs)
      -- go (EPrim a f xs)   = EPrim a f (map go xs) 
      -- go (EIf a e1 e2 e3) = EIf a (go e1) (go e2) (go e3)
      -- go (ESeq a e1 e2)   = ESeq a (go e1) (go e2) 
      -- go (EText a e)      = EText a (go e)
      -- go (EReify a e1 e2) = EReify a (go e1) (go e2)
      -- go (EAltL a e1 e2)  = EAltL  a (go e1) (go e2)
      -- go (EInt a i)       = EInt a i 
      -- go (EString a s)    = EString a s 

{-| 
  Inlining function 
-}
inline :: (Prog a, [(Name,[Type],Type)]) -> (Prog a, [(Name,[Type],Type)])
inline (prog, sign) = ( inlinedProg++unprocedProg, sign )
    where
      -- inlinedProg = [ inlineD d | 
      --                 d@(Decl _ f _ _) <- prog, 
      --                 isPpr f || isRecursive f || not (takeDocuments f) ]
      mainFunc = head [ f | Decl _ f _ _ <- prog ]

      inlinedProg = 
          [ inlineD d | d@(Decl _ f _ _) <- prog, not (inlinable f) ]

      unprocedProg =
          [ d | d@(Decl _ f _ _) <- prog, not (elem f lhs), elem f rhs ]
          where
            lhs = [ f | Decl _ f _ _ <- inlinedProg ]
            rhs = concat [ freeVarsRHS d | d <- inlinedProg ] 

      sccCallGraph =
          stronglyConnComp $
            [ (f,f,freeVarsRHS d) | d@(Decl _ f ps e) <- prog ]

      recursiveMap = concatMap f sccCallGraph 
              where
                f (AcyclicSCC g) = [(g,False)]
                f (CyclicSCC gs) = zip gs (cycle [True])

      isRecursive f = fromJust $ lookup f recursiveMap 

      -- Inlinable functions (1) do not have pattern matches in any arguments
      --                     (2) are not recursive 
      --                     (3) takes more than one argument 
      --                            
      inlinable :: Name -> Bool 
      inlinable f 
          | f == mainFunc 
              = False 
          | null [ ps | Decl _ g ps _ <- prog, g == f] =
            False 
          | otherwise =
            all (\ps -> null [ c | PCon _ c _ <- ps ] &&
                        not (isRecursive f) && (length ps <= 1 ))
             [ ps | Decl _ g ps _ <- prog, g == f]

      -- takeDocuments f = 
      --     case lookup f tMap of 
      --       Just (ts,t) ->
      --           not $ null [ 1 | TyDocument <- ts ]
      --       Nothing -> 
      --           False 

      inlineD (Decl i f ps e) = Decl i f ps (inlineE (concatMap freeVarsP ps) [] e)

      inlineE bv env (EVar a f es) 
          | f `elem` bv = 
              EVar a f es 
          | f `elem` (map fst env) = 
              fromJust $ lookup f env 
          | inlinable f = 
              case [ (ps',e') | Decl _ f' ps' e' <- prog, f' == f  ] of 
                [(ps',e')] -> -- ps' must be variables
                    let newEnv = zip [ x | PVar _ x <- ps' ] (map (inlineE bv env ) es) 
                    in inlineE [] newEnv e' 
                [] ->
                    error $ "No such function: " ++ show f ++ ".\n"
                _ ->
                    error $ "The function " ++ show f ++ " has multiple definitions.\n" 
          | otherwise =
              EVar a f (map (inlineE bv env) es)
      inlineE bv env e =
          composOp (inlineE bv env) e 
      -- inlineE bv env (ECon a c es) = ECon a c (map (inlineE bv env) es)
      -- inlineE bv env (EIf a e1 e2 e3) = 
      --     EIf a (inlineE bv env e1) (inlineE bv env e2) (inlineE bv env e3)
      -- inlineE bv env (EPrim a f es) =
      --     EPrim a f (map (inlineE bv env) es)
      -- inlineE bv env (ESeq a e1 e2) = ESeq a (inlineE bv env e1) (inlineE bv env e2)
      -- -- inlineE bv env (ELine a) = ELine a 
      -- -- inlineE bv env (EGroup a e) = EGroup a (inlineE bv env e)
      -- -- inlineE bv env (ENest a e1 e2) = ENest a (inlineE bv env e1) (inlineE bv env e2)
      -- inlineE bv env (EText a e) = EText a (inlineE bv env e) 
      -- inlineE bv env (EAltL a e1 e2) = EAltL a (inlineE bv env e1) (inlineE bv env e2)
      -- inlineE bv env (EReify a e1 e2) = EReify a (inlineE bv env e1) (inlineE bv env e2)
      -- inlineE bv env (EInt a i) = EInt a i 
      -- inlineE bv env (EString a s) = EString a s 

      tMap = [ (f,(ts,t)) | (f,ts,t) <- sign ]
      isPpr f = 
          case lookup f tMap of
            Just (ts,t) -> elem TyInput ts 
            _           -> False 



          