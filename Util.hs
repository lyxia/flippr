module Util where 

import Data.Typeable 
import Data.Dynamic 

import Data.Maybe 

import Text.WadlerPpr 

type CodeSpan = ( (Int,Int,Int), -- Starting position, line number, and column number
                  (Int,Int,Int)  -- Ending   position, line number, and column number 
                ) 

-- assign :: Typeable a => String -> a -> [(String,Dynamic)]
-- assign  x  s  = [(x,toDyn s)]
                
-- assignS :: [String] -> [Dynamic] -> [(String,Dynamic)]
-- assignS xs ts = zip xs ts
                
-- ignore _ = []
ignoreU _ = () 
           
-- resolve env x = fromJust $ fromDynamic $ fromJust $ lookup x env

charOf f = text $ (:[]) $ head $ filter f [ minBound :: Char .. maxBound :: Char ]

andalso f g x = f x && g x 
orelse  f g x = f x || g x
butnot  f g x = f x && not (g x)
anyChar _ = True
none    _ = False 
is c = (== c) 
           
infixr 5 <+
x <+ y = x
infixr 8 @@
x @@ _ = text x

data Bij a b = Bij { bij :: a -> b, inv :: b -> a }

invMap n l = [ (x, (toDyn . inv n . fromJust . fromDynamic) y ) | (x,y) <- l ]