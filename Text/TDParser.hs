{-|
A Parser Combinator by R.A.Frost's Top-down parsing 

Some of codes are just borrowed from 
http://cs.uwindsor.ca/~richard/PUBLICATIONS/Demonstration.hs
-}

{-# LANGUAGE FlexibleContexts, RankNTypes, GADTs, KindSignatures  #-}



module Text.TDParser where 

import Data.IntMap (IntMap)
import qualified Data.IntMap as I 
import Data.IntSet (IntSet)
import qualified Data.IntSet as S 

import Data.Dynamic 
import Data.Typeable 

import Data.Monoid 

import Data.IORef 
import Control.Monad 
import Control.Applicative 

import Data.Maybe (fromJust) 

import System.IO.Unsafe 
import Control.DeepSeq 

import Debug.Trace 

import Data.CharSet (CharSet)
import qualified Data.CharSet as CS 
----------------------------------------------------


type Key    = Int 
type KeyMap = IntMap 
type Pos    = Int 
type PosMap = IntMap 

type Enumerate a = [a] -- FIXME: should be abstracted later 

-- Tag for Efficient Ordering and Comparison
data Tag m k a
    = Tag 
     {-# UNPACK #-} !Pos -- Next position used as sorting key 
      (InputBuffer m k) 
      String
      a 

data St = Err Int Int | Ok 
          deriving Show 

instance Monoid St where 
    mempty = Err 0 0 
    mappend (Ok)  _         = Ok  
    mappend (Err _ _) (Ok)    = Ok 
    mappend (Err l1 c1) (Err l2 c2) 
        | l1 == l2 = Err l1 (max c1 c2)
        | l1 >  l2 = Err l1 c1
        | l1 <  l2 = Err l2 c2 

type ErrorMsg = String 
data E m k a = E St [ Tag m k (m a) ]


singletonE ib s r = 
    E Ok [ Tag (position ib) ib s (return r) ]

instance Functor m => Functor (E m k) where
    fmap f (E s ls) = E s [ Tag p ib s (fmap f rs) | Tag p ib s rs <- ls ]


emptyE (l,c) = E (Err l c) []
mergeE (E s1 a) (E s2 b) = E (s1 <> s2) $ sortedMerge a b 

-- sortedMerge :: Monoid a => [Tag m k a] -> [Tag m k a] -> [Tag m k a]
sortedMerge [] bs = bs
sortedMerge as [] = as
sortedMerge as@(ia@(Tag i ii s a):at) bs@(jb@(Tag j jj t b):bt)
 = case i `compare` j of
      LT -> ia : sortedMerge at bs
      EQ -> Tag i ii s (mplus a b) : sortedMerge at bt
      GT -> jb : sortedMerge as bt

type CurtailingNTs = IntSet
type UpResult m k a   = (CurtailingNTs, E m k a) 

type KeyContext = [ (Key, Int) ] -- Key & Counter pairs 

data Stored m k 
    = Stored { storedCuts    :: CurtailingNTs
             , storedResults :: E m k Dynamic 
             , storedContext :: KeyContext 
             }

type Memo m k = KeyMap (Stored m k)

data InputBuffer m k = InputBuffer
    { restString  :: String
    , memoRefs    :: [IORef (Memo m k)]
    , position    :: !Pos
    , lineCount   :: Int
    , columnCount :: Int 
    , restLength  :: Int }

instance Show (InputBuffer m k) where
    show (InputBuffer _ _ p l c r) = show (p,l,c,r)


type Memoized a = IO a 

type ParseFunc m k a 
    = (MonadPlus m, Functor m) => 
      KeyContext -> InputBuffer m k -> Memoized (UpResult m k a)


data Parser (m :: * -> *) k a where 
    ParseEmpty    :: Parser m k a 
    ParsePure     :: a -> Parser m k a 
    ParseCharOf   :: CharSet -> Parser m k Char 
    ParseText     :: String -> Parser m k String 
    ParseAlt      :: Parser m k a -> Parser m k a -> Parser m k a 
    ParseSeq      :: Parser m k (a->b) -> Parser m k a -> Parser m k b 
    ParseMemo     :: Typeable a => Key -> Parser m k a -> Parser m k a 
    ParseParsed   :: Parser m k a      -> Parser m k String 
    ParsePosition :: Parser m k (Int,Int,Int)
    -- FIXME: Use the following for efficiency?
    -- ParseCAlt     :: [(Char, Parser m k a)] -> Parser m k a 
    

instance Functor (Parser m k) where 
    fmap f x = ParseSeq (ParsePure f) x 

instance Applicative (Parser m k) where 
    pure    = ParsePure 
    f <*> x = ParseSeq f x 
              
instance Alternative (Parser m k) where 
    empty  = ParseEmpty 
    (<|>)  = ParseAlt 
    -- do not use many and some 

text :: String -> Parser m k String 
text = ParseText 

memoize :: (Typeable a, Enum k) => k -> Parser m k a -> Parser m k a 
memoize k p = ParseMemo (fromEnum k) p

charOf :: CharSet -> Parser m k Char 
charOf = ParseCharOf 

parsedString = ParseParsed 

currentPosition :: Parser m k (Int,Int,Int)
currentPosition = ParsePosition

-------------------------------
emptyCuts = S.empty 

data Flag = End | Middle  

noSolution (l,c) = ( emptyCuts, emptyE (l,c) ) 

parseErrorMsg (lc,cc) = "Parse Error at line " ++ show (lc+1) ++ ":" ++ show (cc+1) ++ "." 

parseErr  ib    = parseErr' (lineCount ib, columnCount ib)
parseErr' (l,c) = noSolution (l,c)

errorE  ib    = errorE' (lineCount ib, columnCount ib)
errorE' (l,c) = emptyE (l,c) 

nextLC (l,c) '\n' = (l+1,0)
nextLC (l,c) _    = (l,c+1)

nextLCs (l,c) str = foldl (nextLC) (l,c) str
  
showStatus (E s _) = show s 
--------------------------------------------------------------------------
parse :: Flag -> Parser m k a -> ParseFunc m k a 

parse _ ParseEmpty context ib = return $ parseErr ib 

parse _ (ParsePure x) context ib = return $ (emptyCuts, singletonE ib "" x) 

parse f (ParseCharOf p) context (InputBuffer rest memos pos lc cc rlen) =
    case f of 
      End -> 
          case rest of 
            [x] | x `CS.member` p -> 
                    let (lc',cc') = nextLC (lc,cc) x
                        nextIB = InputBuffer [] [] (pos + rlen) lc' cc' 0 
                    in return $ ( emptyCuts, singletonE nextIB [x] x)
            _ -> 
                return $ parseErr' (lc,cc)
      Middle -> 
          case rest of 
            x:xs | x `CS.member` p -> 
                     let (lc',cc') = nextLC (lc,cc) x
                         nextIB = InputBuffer xs (tail memos) (pos+1) lc' cc' (rlen-1)
                     in return $ ( emptyCuts, singletonE nextIB [x] x) 
            _ -> 
                return $ parseErr' (lc,cc)

parse f (ParseText s) context (InputBuffer rest memos pos lc cc rlen) =
    case f of 
      End -> 
          if s == rest then 
              let (lc',cc') = nextLCs (lc,cc) s 
                  nextIB = InputBuffer [] [] (pos + rlen) lc' cc' 0 
              in return $ ( emptyCuts, singletonE nextIB s s )
          else 
              return $ parseErr' (lc,cc)
      Middle ->
          let len = length s 
              t   = take len rest 
          in if s == t then 
                 let (lc',cc') = nextLCs (lc,cc) s 
                     nextIB = InputBuffer (drop len rest) 
                                          (drop len memos) 
                                          (pos + len)
                                          lc' cc' 
                                          (rlen - len)
                 in return $ ( emptyCuts
                             , singletonE nextIB s s )
             else
                 return $ parseErr' (lc,cc)
      
parse f (ParseAlt p q) context ib = 
    do { (cut1, r1) <- parse f p context ib 
       ; (cut2, r2) <- parse f q context ib 
       ; return ( S.union cut1 cut2 
                , mergeE r1 r2 ) }
                          
parse f (ParseSeq p q) context ib =
    do { (cut, E e pResults) <- parse Middle p context ib 
       ; let joinForFirst (Tag nextPos nextIB s ps) =
                 do { let nullP = position ib == nextPos 
                    ; let newContext | nullP     = context 
                                     | otherwise = []
                    ; (newCuts, E e qResults) <- parse f q newContext nextIB 
                    ; let outCuts | nullP     = newCuts `S.union` cut
                                  | otherwise = cut 
                    ; return ( outCuts, E e [ Tag j jb (s++t) (appE ps qs) | Tag j jb t qs <- qResults ] ) }
             joinForRest prevResults (Tag nestPos nextIB s ps) =
                 do { (_,E e qResults) <- parse f q [] nextIB 
                    ; return $ E e [ Tag j jb (s++t) (appE ps qs) | Tag j jb t qs <- qResults ]
                                 `mergeE` prevResults }
       ; case pResults of 
               []     -> return ( cut, E e [] ) 
               r:rs   -> do { (outCuts, firstRes) <- joinForFirst r 
                            ; outResults <- foldM joinForRest firstRes rs 
                            ; return (outCuts, outResults) }}
    where
      appE ps qs = 
          do { p <- ps 
             ; q <- qs 
             ; return $ p q }

parse f (ParseMemo etag p) context ib = 
    do { let memoRef = head $ memoRefs ib 
       ; mTable <- readIORef memoRef 
       ; case lookupT key context mTable of 
           Just res ->
               return res 
           Nothing 
               | funcCount context > restLength ib ->
                   return (S.singleton key, errorE ib )
               | otherwise ->
                   do { let newDownContext = incContext key context 
                      ; (upCuts, results) <- parse f p newDownContext ib
                      ; let toStore = Stored upCuts 
                                             (fmap toDyn results)
                                             (pruneContext upCuts context)
                      ; modifyIORef memoRef (update toStore)
                      ; return (upCuts, results) }}
    where
      key = let b = fromEnum etag in 
            case f of 
              Middle -> 2 * b 
              End    -> 2 * b + 1
      update res kMap 
          = I.insert key res kMap 
      funcCount cs = case lookup key cs of 
                       Nothing  -> 0 
                       Just cnt -> cnt 

parse f (ParseParsed p) context ib =
    do { (cut, E s pResults) <- parse f p context ib 
       ; return $ (cut, E s $ map h pResults ) }
    where
      h (Tag p ib s _) = Tag p ib s (return s)

parse f (ParsePosition) context ib =
    return ( emptyCuts, singletonE ib "" (position ib, lineCount ib, columnCount ib) )


-- eos :: Parser m k () 
-- eos = 
--     Parser $ \_ ib -> 
--         if restLength ib == 0 then 
--             return ( emptyCuts, singletonE ib () ) 
--         else
--             return ( emptyCuts, emptyE ) 

-- empty :: Parser m k () 
-- empty =
--     Parser $ \context ib -> return $ (emptyCuts, singletonE ib ())

-- text :: String -> Parser m k String 
-- text s = Parser go 
--     where 
--       go context (InputBuffer rest memos pos rlen) = 
--           if s == t then 
--               let nextIB = InputBuffer (drop len rest) 
--                                        (drop len memos) 
--                                        (pos + len)
--                                        (rlen - len)
--               in return $ ( emptyCuts
--                           , singletonE nextIB s )
--           else
--               return $ ( emptyCuts 
--                        , E [] )
--           where
--             len = length s 
--             t = take len rest 
         
          
-- (<+>) :: Parser m k a -> Parser m k a -> Parser m k a 
-- (Parser p) <+> (Parser q)
--     = Parser $ \context ib ->
      -- do { (cut1, r1) <- p context ib 
      --    ; (cut2, r2) <- q context ib 
      --    ; return ( S.union cut1 cut2 
      --             , mergeE r1 r2 ) }


-- mixE :: Monad m => m a -> m b -> m (a,b) 
-- mixE x y = do { a <- x 
--               ; b <- y 
--               ; return (a,b)}

-- (**>) :: MonadPlus m  => Parser m k a -> Parser m k b -> Parser m k (a,b) 
-- (**>) (Parser p) (Parser q) = Parser go 
--     where 
--       go context ib =
--           do (cut, E pResults) <- p context ib 
--              let joinForFirst (Tag nextPos nextIB ps) =
--                      do let nullP = position ib == nextPos 
--                         let newContext | nullP     = context 
--                                        | otherwise = []
--                         (newCuts, E qResults) <- q newContext nextIB 
--                         let outCuts | nullP     = newCuts `S.union` cut
--                                     | otherwise = cut 
--                         return ( outCuts, E [ Tag j jb (mixE ps qs) | Tag j jb qs <- qResults ] )
--                  joinForRest prevResults (Tag nestPos nextIB ps) =
--                      do (_,E qResults) <- q [] nextIB 
--                         return $ E [ Tag j jb (mixE ps qs) | Tag j jb qs <- qResults ]
--                                  `mergeE` prevResults 
--              case pResults of 
--                []     -> return ( cut, emptyE ) 
--                r:rs   -> do (outCuts, firstRes) <- joinForFirst r 
--                             outResults <- foldM joinForRest firstRes rs 
--                             return (outCuts, outResults) 
                            
-- instance Functor m => Functor (Parser m k) where 
--     fmap f (Parser p) = Parser $ \context ib -> 
--                         do { (cut,r) <- p context ib 
--                            ; return $ (cut, fmap f r) }             

-- instance (MonadPlus m, Functor m) => Applicative (Parser m k) where 
--     pure x  = fmap (\_ -> x) empty 
--     f <*> x = fmap (uncurry ($)) (f **> x)

-- memoize :: (Enum k, Typeable a, MonadPlus m, Functor m )
--            => k -> Parser m k a -> Parser m k a 
-- memoize etag (Parser p) = Parser go
--     where
--       key = fromEnum etag
--       go context ib =
--           do { let memoRef = head $ memoRefs ib 
--              ; mTable <- readIORef memoRef 
--              ; case lookupT key context mTable of 
--                  Just res ->
--                      return res 
--                  Nothing 
--                      | funcCount context > restLength ib ->
--                          return (S.singleton key, E []) 
--                      | otherwise ->
--                          do { let newDownContext = incContext key context 
--                             ; (upCuts, results) <- p newDownContext ib
--                             ; let toStore = Stored upCuts 
--                                                    (fmap toDyn results)
--                                                    (pruneContext upCuts context)
--                             ; modifyIORef memoRef (update toStore)
--                             ; return (upCuts, results) }}
--           where
--             update res kMap 
--                 = I.insert key res kMap 
--             funcCount cs = case lookup key cs of 
--                              Nothing  -> 0 
--                              Just cnt -> cnt 

                           
pruneContext rs _
 | S.null rs = []
pruneContext rs ctxt
 = [ nc | nc@(n,c) <- ctxt, n `S.member` rs ]
   
incContext name []                         = [(name,1)]
incContext name (nc@(n,c):ncs) | n == name = (n,c + 1) : ncs 
                               | otherwise = nc : incContext name ncs
                        

lookupT :: (Functor m, Typeable a) => Key -> KeyContext -> KeyMap (Stored m k) 
           -> Maybe (UpResult m k a)
lookupT key current mTable
    = do { memo  <- I.lookup key mTable
         ; let cuts    = storedCuts memo
         ; let results = fmap fromDyn' $ storedResults memo 
         ; if S.null cuts then 
               return $ (cuts, results)
           else if canReuse current (storedContext memo) then 
                    Just (cuts, results) 
                else 
                    Nothing }
   where
     fromDyn' = fromJust . fromDynamic 

     canReuse :: KeyContext -> KeyContext -> Bool
     canReuse current stored 
         = and [ or [ cc >= sc 
                          | (cn,cc) <- current, sn == cn ]
                 | (sn,sc) <- stored ]

runParser p string = 
    unsafePerformIO $ 
       do { rs <- mapM (\_ -> newIORef I.empty) (undefined:string)
          ; let len = length string 
          ; let ib = InputBuffer string rs 0 0 0 len 
          ; (_,E s results) <- len `seq`  parse End p [] ib 
          ; case s of 
              Ok -> 
                  return $ msum [ rs | Tag i _ _ rs <- results, i == len ] 
              Err l c ->
                  fail $ parseErrorMsg (l,c)
          }
    

-- runParser :: MonadPlus m => Parser m k a -> String -> m a 
-- runParser (Parser p) string = 
--     unsafePerformIO $ 
--        do { rs <- mapM (\_ -> newIORef I.empty) string 
--           ; let len = length string 
--           ; let ib = InputBuffer string rs 0 len 
--           ; (_,E results) <- len `seq`  p [] ib 
--           ; return $ msum [ rs | Tag i _ rs <- results, i == len ] }
                    



-- parenLang :: (Functor m, MonadPlus m) => Parser m Int Integer 
-- parenLang = s 
--     where
--       s = memoize 0 $ 
--             ((\_ a _ b -> a + b + 1) <$> 
--                   text "(" <*> s <*> text ")" <*> s)
--             <+>
--             ((\a -> 0) <$> text "s")

-- testData n = p n 
--     where
--       p 0 = "s"
--       p n = "(" ++ p (n-1) ++ ")" ++ p (n-1) 
          

-- main = runParser parenLang (testData 18)
    
