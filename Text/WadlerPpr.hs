{-|
A minimal implementation of Wadler's pretty-printing library for 
our system. 

All the codes are borrowed from his paper.
-}

-- TODO: Should we change it with something from Hackage?

module Text.WadlerPpr
    (
     Doc, pretty, nil, (<>), nest, text, line, group, 
     (<+>), (</>), (<+/>), foldDoc, parens, brackets, 
     
    )
    where 

import Data.Monoid 

infixr 5 :<|>
infixr 6 :<>
-- infixr 6 <>

data Doc = NIL
         | Doc :<> Doc
         | NEST Int Doc 
         | TEXT String 
         | LINE 
         | Doc :<|> Doc 

data DOC = Nil 
         | Text String DOC 
         | Line Int  DOC

instance Monoid Doc where 
    mappend = (:<>)
    mempty  = NIL 

nil      = NIL 
-- x <> y   = x :<> y 
nest i x = NEST i x
text s   = TEXT s 
line     = LINE 

group x = flatten x :<|> x 

flatten NIL        = NIL
flatten (x :<> y)  = flatten x :<> flatten y 
flatten (NEST i x) = NEST i (flatten x) 
flatten (TEXT s)   = TEXT s 
flatten LINE       = TEXT " " 
flatten (x :<|> y) = flatten x            

layout Nil = ""
layout (Text s x) = s ++ layout x 
layout (Line i x) = '\n' : replicate i ' ' ++ layout x 

best w k x = be w k [ (0,x) ] 

be w k [] = Nil 
be w k ( (i,d):z ) =
    case d of 
      NIL      -> be w k z 
      x :<> y  -> be w k ( (i,x):(i,y):z )
      NEST j x -> be w k ( (i+j,x):z )
      TEXT s   -> Text s $ be w (k+length s) z 
      LINE     -> Line i $ be w i z 
      x :<|> y -> better w k (be w k ((i,x):z))
                             (be w k ((i,y):z))


better w k x y = if fits (w-k) x then x else y 

fits w x | w < 0  = False 
fits w Nil        = True 
fits w (Text s x) = fits (w - length s) x 
fits w (Line i x) = True 

pretty w x = layout (best w 0 x) 

instance (Show Doc) where 
    show d = pretty 80 d 

----------- Util 
x <+> y = x <> text " " <> y 
x </> y = x <> line <> y 

x <+/> y = x <> (group line) <> y 

foldDoc f []     = nil 
foldDoc f [x]    = x 
foldDoc f (x:xs) = f x (foldDoc f xs) 

parens   d = text "(" <> d <> text ")" 
brackets d = text "[" <> d <> text "]"


