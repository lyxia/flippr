{-# LANGUAGE DeriveDataTypeable #-}

module Data.CharSet.Base where 


import Data.Typeable
import Data.Char 
import Text.Printf 

-- | Sets of characters.
--   e.g., The character class [A-Z] is represented by |CharSet ['A',succ 'Z']|
--   The list |cs| of |CharSet cs| must be sorted. 
newtype CharSet = CharSet { runCS :: [Char] }
    deriving (Eq,Ord,Typeable)

{-# INLINE convertToSet #-}
convertToSet f = 
    CharSet $ go [minBound :: Char .. maxBound :: Char ] False
    where
      go [] True  = []
      go [] False = []
      go (x:xs) b =
          if f x /= b then 
              x:go xs (f x)
          else
              go xs (f x)
             
charSetRange s e =
    if e == maxBound then 
        CharSet [s]
    else
        CharSet [s,succ e]

instance Show CharSet where 
    show (CharSet b) = "["++showCharSet b++"]"
    
showCharSet []                  = "*empty*"
showCharSet [x] | x == minBound = "*all*"
                | otherwise     = showC x++"-" ++ showC maxBound 
showCharSet xs = go xs Nothing
    where
      go [] (Just x)  = showC x++"-"++showC maxBound 
      go [] Nothing   = ""
      go (x:xs) (Just y) 
        | succ y == x = showC y++go xs Nothing 
        | otherwise   = showC y++"-"++showC (pred x) ++ go xs Nothing 
      go (x:xs) Nothing = go xs (Just x)
      
showC c | isPrint c && isAscii c = [c]
        | otherwise              = printf "\\%0d" (ord c)
      

intersect (CharSet bs1) (CharSet bs2) = CharSet $ merge bs1 bs2 False False
    where
      merge [] bs2 f1 f2 = if f1 then bs2 else []
      merge bs1 [] f1 f2 = if f2 then bs1 else []
      merge (b1:bs1) (b2:bs2) f1 f2
          | b1 < b2 =
              if (f1 && f2) /= (not f1 && f2) then 
                  b1:merge bs1 (b2:bs2) (not f1) f2
              else
                  merge bs1 (b2:bs2) (not f1) f2
          | b1 > b2 =
              if (f1 && f2) /= (f1 && not f2) then 
                  b2:merge (b1:bs1) bs2 f1 (not f2)
              else
                  merge (b1:bs1) bs2 f1 (not f2)
          | otherwise = 
              if (f1 && f2) /= (not f1 && not f2) then 
                  b1:merge bs1 bs2 (not f1) (not f2)
              else
                  merge bs1 bs2 (not f1) (not f2)

union (CharSet bs1) (CharSet bs2) = CharSet $ merge bs1 bs2 False False
    where
      merge [] bs2 f1 f2 = if f1 then [] else bs2 -- right?
      merge bs1 [] f1 f2 = if f2 then [] else bs1 -- right?
      merge (b1:bs1) (b2:bs2) f1 f2
          | b1 < b2 =
              if (f1 || f2) /= (not f1 || f2) then 
                  b1:merge bs1 (b2:bs2) (not f1) f2
              else
                  merge bs1 (b2:bs2) (not f1) f2
          | b1 > b2 =
              if (f1 || f2) /= (f1 || not f2) then 
                  b2:merge (b1:bs1) bs2 f1 (not f2)
              else
                  merge (b1:bs1) bs2 f1 (not f2)
          | otherwise = 
              if (f1 || f2) /= (not f1 || not f2) then 
                  b1:merge bs1 bs2 (not f1) (not f2)
              else
                  merge bs1 bs2 (not f1) (not f2)

neg (CharSet [])  = universal
neg (CharSet [c]) = 
    if c == minBound then 
        CharSet []
    else 
        CharSet [minBound,c]
neg (CharSet (s@(c:cs))) =
  let start = if c == minBound then 
                []
              else
                [minBound, c]
  in CharSet $ start ++ cs

isEmpty (CharSet []) = True
isEmpty _  = False
    
singleton s = CharSet [s,succ s]
empty       = CharSet []
universal   = CharSet [ minBound :: Char ]

fromList = foldr union empty . map singleton 

member c (CharSet xs) =
    go xs 
    where
      go []  = False
      go [x] = c >= x
      go (s:e:xs) 
          | c < s           = False 
          | c >= s && c < e = True 
          | c >= e          = go xs

