{-|
This file implements Compos by
  Björn Bringert, Aarne Ranta: 
  A pattern for almost compositional functions. 
  J. Funct. Program. 18(5-6): 567-598 (2008).

All the codes in this file are borrowed from the paper. 

This is a minimal implementation for our system. 
-}

module Data.Composable where 

import Control.Monad
import Control.Monad.Identity 
import Control.Applicative
import Data.Monoid 

class Composable a where 
    compos :: Applicative f =>
              (a -> f a) -> a -> f a

composOp :: Composable a => (a -> a) -> a -> a
composOp f = runIdentity . compos (Identity . f) 

composFold :: (Composable a, Monoid o) => (a -> o) -> a -> o 
composFold f = getConst . compos (Const . f)

composM :: (Composable a, Monad m) => (a -> m a) -> a -> m a
composM f = unwrapMonad . compos (WrapMonad . f)
